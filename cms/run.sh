#!/bin/bash

set -x

# Start the scheduler
python3 -m cms.cron &

# Start the web server
uvicorn cms.main:app --port 3030 --host 0.0.0.0
