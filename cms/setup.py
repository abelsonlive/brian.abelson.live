import os
from setuptools import setup, find_packages

reqs = os.path.abspath(
    os.path.join(os.path.dirname(__file__), "requirements.txt")
)
with open(reqs) as f:
    install_requires = [req.strip().split("==")[0] for req in f]

config = {
    "name": "brian-abelson-live-cms",
    "version": "0.0.1",
    "packages": find_packages(),
    "install_requires": install_requires,
    "author": "Brian Abelson",
    "author_email": "brian@abelson.live",
    "description": "Custom CMS for brian.abelson.live",
    "url": "http://brian.abelson.live",
}

setup(**config)
