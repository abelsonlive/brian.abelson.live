import logging
from typing import List, Annotated

import uvicorn
from fastapi import FastAPI, Form, UploadFile, HTTPException, status
from fastapi.responses import RedirectResponse
from fastapi.middleware.cors import CORSMiddleware

from cms.settings import APIKEY, CMS_PATH
from cms import jekyll
from cms import repo
from cms.util import resize_all_media

log = logging.getLogger(__name__)

app = FastAPI(debug=True)
app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        "http://brian.abelson.live",
        "https://brian.abelson.live",
        "http://localhost:4000",
        "http://127.0.0.1:4000",
        "https://localhost:4000",
        "https://127.0.0.1:4000",
        "http://brian.local",
    ],
    allow_credentials=False,
    allow_methods=["*"],
    allow_headers=["*"],
)


def check_api_key(apikey: str):
    if apikey != APIKEY:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid API Key"
        )


@app.post("/post")
def post(
    apikey: Annotated[str, Form()],
    site_url: Annotated[str, Form()],
    title: Annotated[str, Form()],
    summary: Annotated[str, Form()],
    content: Annotated[str, Form()],
    categories: Annotated[List[str], Form()] = [],
    new_categories: Annotated[str, Form()] = "",
    image: UploadFile | None = None,
    video: UploadFile | None = None,
    audio: UploadFile | None = None,
    published: Annotated[bool, Form()] = True,
):
    """
    """
    combined_categories = []
    for cat in categories:
        if cat != "new":
            combined_categories.append(cat.strip())
    combined_categories.extend(
        [c.strip() for c in new_categories.split(",") if c.strip()]
    )

    check_api_key(apikey)
    # pull the latest changes from the git repo
    log.info("Cloning git repo")
    repo.clone()
    log.info("Pulling latest changes from git repo")
    repo.pull()
    log.info("Resizing media")
    media = resize_all_media(title, image, video, audio)
    log.info(f"Generating Post: {title}")
    # generate the post

    _, post_url = jekyll.create_post(
        site_url=site_url,
        title=title.strip(),
        summary=summary.strip(),
        content=content.strip(),
        categories=combined_categories,
        image=media.get("image", None),
        video=media.get("video", None),
        published=published,
    )
    # push the changes to the git repo
    log.info("Pushing changes to git repo")
    repo.add_and_commit(f"Added post: {title}")
    repo.push()
    log.info(f"Post generated: {post_url}")
    return RedirectResponse(url=f"{site_url}{CMS_PATH}", status_code=303)

@app.get("/yo")
def health_check():
    """
    :param email: The phone number to validate
    :return: The formatted email
    """
    return {"status": "ok"}

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=3030)