import json
import logging
from typing import List, Optional, Union
from atproto import Client, client_utils

from cms.settings import BSKY_USERNAME, BSKY_PASSWORD
from cms import jekyll, repo

log = logging.getLogger(__name__)
client = Client()

def clean_link(link: str) -> str:
    return link.replace("https://", "").replace("http://", "").replace("www.", "")


def create_post(
    text: str,
    links: Optional[List[Union[str, List[str]]]] = None,
    image: Optional[str] = None,
):
    tb = client_utils.TextBuilder()
    tb.text(text)
    if links:
        tb.text("\n")
        for link in links:
            if not isinstance(link, list):
                link = [link]
            if len(link) == 1:
                link = [clean_link(link[0]), link[0]]
            tb.link(*link)
            tb.text("\n")
    client.login(login=BSKY_USERNAME, password=BSKY_PASSWORD)
    if image:
        with open(image, "rb") as f:
            img_data = f.read()
            resp = client.send_image(text=tb, image=img_data, image_alt="Image")
    else:
        resp = client.send_post(tb)
    return resp


def create_post_from_most_recent_blog_post(raise_errors: bool = False):
    try:
        log.info("Cloning git repo...")
        repo.clone()
        log.info("Pulling latest changes from git repo...")
        repo.pull()
        data = jekyll.get_last_post()
        if data.get('bsky_uri', None):
            log.info("Post already on bsky!")
            return
        text = data.get('summary', None)
        if not text:
            text = data['content'][:150] + '...'
        url = data['url']
        image = data.get('image_abspath', None)
        resp = create_post(text, [url], image)
        log.info(f"Posted {data['url']} to bsky!")
        jekyll.update_post(data['file_name'], bsky_uri=resp.uri)
        repo.add_and_commit(f"Posted {data['url']} to bsky")
        log.info("Pushing changes to git repo...")
        repo.push()
        return resp
    except Exception as e:
        log.error(e)
        if raise_errors:
            raise e
        return None

if __name__ == "__main__":
    create_post_from_most_recent_blog_post(raise_errors=True)
