import os
import subprocess

from datetime import datetime
from slugify import slugify

from .settings import (
    MAX_IMG_WIDTH,
    MAX_VIDEO_WIDTH,
    IMG_TYPE,
    VIDEO_TYPE,
    AUDIO_TYPE,
    POST_ASSET_DIR,
    POST_ASSET_PATH,
    VIDEO_ASSET_DIR,
    AUDIO_ASSET_DIR,
    VIDEO_ASSET_PATH,
    AUDIO_ASSET_PATH,
)


def resize_media(filename, type="image", include_date=True, remove_original=True):
    """
    Resize a media file to a maximum width.
    """
    dir = os.path.dirname(filename)
    now = datetime.now()
    slug = (
        now.strftime(r"%Y-%m-%d-")
        if include_date
        else "" + slugify(".".join(filename.split("/")[-1].split(".")[0:-1]))
    )
    if type == "image":
        small_filename = os.path.join(dir, f"{slug}-small.{IMG_TYPE}")
        args = ["-vf", f"scale={MAX_IMG_WIDTH}:-1", small_filename]
    elif type == "video":
        small_filename = os.path.join(dir, f"{slug}-small.{VIDEO_TYPE}")
        args = ["-vf", f"scale={MAX_VIDEO_WIDTH}:-1", small_filename]
    elif type == "audio":
        small_filename = os.path.join(dir, f"{slug}-small.{AUDIO_TYPE}")
        args = [small_filename]
    else:
        raise ValueError(f"Unknown file type: {filename}")

    cmd = ["ffmpeg", "-y", "-i", filename] + args
    # run the command and check for errors
    try:
        subprocess.run(cmd, check=True)
        if remove_original:
            os.remove(filename)
    except subprocess.CalledProcessError as e:
        raise e
    return small_filename


def resize_all_media(title, image, video, audio):
    media = {}
    files = [
        (image, "image", POST_ASSET_DIR, POST_ASSET_PATH),
        (video, "video", VIDEO_ASSET_DIR, VIDEO_ASSET_PATH),
        (audio, "audio", AUDIO_ASSET_DIR, AUDIO_ASSET_PATH),
    ]
    for fh, type, local_dir, asset_path in files:
        if fh and fh.filename:
            ext = fh.filename.split(".")[-1]
            filepath = f"{local_dir}/{slugify(title)}.{ext}"
            with open(filepath, "wb") as buffer:
                buffer.write(fh.file.read())
            small_filepath = resize_media(filepath, type)
            asset_path = f"{asset_path}/{small_filepath.split('/')[-1]}"
            media[type] = asset_path
    return media


if __name__ == "__main__":
    import sys

    filename = sys.argv[1]
    type = "image"
    if filename.lower().endswith(".mp4") or filename.lower().endswith(".mov"):
        type = "video"
    resize_media(filename, type, include_date=False)
