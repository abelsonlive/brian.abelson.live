import os
from datetime import datetime
from typing import Union
from zoneinfo import ZoneInfo

import yaml
from slugify import slugify

from .settings import POST_PATH, POST_DIR, SITE_URL, GIT_LOCAL_PATH

JEKYLL_DATE_FORMAT = r"%Y-%m-%d %H:%M %z"
JEKYLL_ALT_DATE_FORMAT = r"%Y-%m-%d %H:%M:%S %z"
KEYS_TO_SKIP_IN_FRONT_MATTER = [
    "content",
    "file_name",
    "url",
    "image_abspath",
    "video_abspath",
]


def format_post_date(date: Union[str, datetime]) -> datetime:
    if not isinstance(date, datetime):
        try:
            date = datetime.strptime(date, JEKYLL_DATE_FORMAT)
        except ValueError:
            date = datetime.strptime(date, JEKYLL_ALT_DATE_FORMAT)
    # force etc time zone if offset-naive
    if date.tzinfo is None or date.tzinfo.utcoffset(date) is None:
        date = date.replace(tzinfo=ZoneInfo("America/New_York"))
    return date


def format_post_url(file_name: str, data: dict):
    if "date" not in data:
        raise ValueError("Post data must contain a 'date' key.")
    date = format_post_date(data["date"])
    html_file_name = (
        file_name.split("/")[-1]
        .replace(".md", ".html")
        .replace(date.strftime(r"%Y-%m-%d-"), "")
    )
    return f"{SITE_URL}{POST_PATH}{date.strftime(r'%Y/%m/%d')}/{html_file_name}"


def read_post(file_name: str, git_local_path: str = GIT_LOCAL_PATH) -> dict:
    """
    Load a post from the _posts directory.
    """
    with open(file_name, "r") as post_file:
        raw_post = post_file.read()
    front_matter = raw_post.split("---\n")[1].strip()
    content = raw_post.split("---\n")[2].strip()
    data = yaml.safe_load(front_matter)
    data["content"] = content
    data["file_name"] = file_name
    data["url"] = format_post_url(file_name, data)
    if data.get("image", None):
        data["image_abspath"] = git_local_path + data["image"]
    if data.get("video", None):
        data["video_abspath"] = git_local_path + data["video"]

    return data


def update_post(file_name: str, **kwargs) -> None:
    """
    Update a post in the _posts directory.
    """

    post = read_post(file_name)
    post.update(kwargs)
    other_keys = {k: post.pop(k, None) for k in KEYS_TO_SKIP_IN_FRONT_MATTER}
    other_keys["file_name"] = other_keys.get("file_name", file_name)
    updated_post = (
        "---\n" + yaml.dump(post, sort_keys=False) + "---\n" + other_keys["content"]
    )
    with open(other_keys["file_name"], "w") as post_file:
        post_file.write(updated_post)

    # add the other keys back to the post
    for k in KEYS_TO_SKIP_IN_FRONT_MATTER:
        post[k] = other_keys[k]
    return post


def list_posts(
    post_dir: str = POST_DIR,
    git_local_path: str = GIT_LOCAL_PATH,
    sort_by_key: str = "date",
    sort_by_fx: callable = format_post_date,
    descending: bool = True,
) -> list:
    """
    List all posts in the _posts directory.
    """
    posts = []
    for post in os.listdir(post_dir):
        if post.endswith(".md"):
            post = os.path.join(post_dir, post)
            posts.append(read_post(post, git_local_path))
    return list(
        sorted(posts, key=lambda x: sort_by_fx(x[sort_by_key]), reverse=descending)
    )


def get_last_post(
    post_dir: str = POST_DIR, git_local_path: str = GIT_LOCAL_PATH
) -> dict:
    """
    Get the most recent post in the _posts directory.
    """
    posts = list_posts(
        post_dir,
        git_local_path,
        sort_by_key="date",
        sort_by_fx=format_post_date,
        descending=True,
    )
    return posts[0] if posts else None


def create_post(
    site_url: str,
    title: str,
    summary: str,
    content: str,
    categories: list,
    image: str = None,
    video: str = None,
    hide_title: bool = False,
    published: bool = True,
) -> str:
    """
    Generate a Jekyll post with customizable YAML front matter and a date-formatted file path.
    """
    # Generate the current date and time
    now = datetime.now(ZoneInfo("America/New_York"))

    # Create the full file path for the post
    file_name = slugify(title)
    post_path = os.path.join(POST_DIR, f"{now.strftime(r'%Y-%m-%d')}-{file_name}.md")
    post_url = f"{site_url}{POST_PATH}{now.strftime(r'%Y/%m/%d')}/{file_name}.html"

    # Create the YAML front matter
    data = {
        "layout": "post",
        "title": title,
        "date": now.strftime(JEKYLL_DATE_FORMAT),
        "summary": summary,
        "categories": [c.lower().strip() for c in categories],
        "hide_title": hide_title,
        "published": published,
    }
    if image:
        data["image"] = image
    if video:
        data["video"] = video

    post_content = "---\n" + yaml.dump(data, sort_keys=False) + "---\n" + content

    # Create the post file and write the content to it
    with open(post_path, "w") as post_file:
        post_file.write(post_content)

    return post_path, post_url
