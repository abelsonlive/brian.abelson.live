import os
import git
import logging

from .settings import GIT_LOCAL_PATH, GIT_REPO_URL, GIT_DEFAULT_BRANCH

log = logging.getLogger(__name__)

def clone():
    """
    Clone the git repository.
    """
    if os.path.exists(os.path.join(GIT_LOCAL_PATH, '.git')):
        log.info(f"Directory '{GIT_LOCAL_PATH}' already exists.")
        return
    git.Repo.clone_from(GIT_REPO_URL, GIT_LOCAL_PATH, branch=GIT_DEFAULT_BRANCH)

def pull():
    """
    Pull changes from a Git repository.
    """
    git.Repo(GIT_LOCAL_PATH).remotes.origin.pull()


def add_and_commit(commit_message):
    """
    Add and commit changes in a Git repository.
    """
    git.Repo(GIT_LOCAL_PATH).git.add(A=True)
    git.Repo(GIT_LOCAL_PATH).index.commit(commit_message)

def push():
    """
    Push changes to a Git repository using a Personal Access Token (PAT).
    """
    git.Repo(GIT_LOCAL_PATH).remotes.origin.push(refspec=GIT_DEFAULT_BRANCH)

if __name__ == "__main__":
    import sys
    cmd = sys.argv[1]
    if cmd == "clone":
        # Clone the repository (for use in Dockerfile)
        log.info("Cloning git repo...")
        clone()
    elif cmd == "pull":
        # Pull the latest changes from the repository
        log.info("Pulling latest changes from git repo...")
        pull()
