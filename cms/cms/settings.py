import os
import logging
import logging.config

from dotenv import load_dotenv

load_dotenv()

# api settings
APIKEY = os.getenv("APIKEY", "yo")

# bsky settings
BSKY_USERNAME = os.getenv("BSKY_USERNAME")
BSKY_PASSWORD = os.getenv("BSKY_PASSWORD")

# repo settings
GIT_ACCESS_TOKEN = os.getenv("GIT_ACCESS_TOKEN")
GIT_LOCAL_PATH = os.path.join(os.path.dirname(__file__), "repo")
GIT_DEFAULT_BRANCH = "main"
GIT_USERNAME = "abelsonlive"
GIT_REPO_URL = f"https://{GIT_USERNAME}:{GIT_ACCESS_TOKEN}@gitlab.com/abelsonlive/brian.abelson.live.git"

# site settings
SITE_URL = "https://brian.abelson.live"
POST_PATH = "/log/"
CMS_PATH = "/cms/"
POST_DIR = os.path.join(GIT_LOCAL_PATH, "_posts")
POST_ASSET_DIR = os.path.join(GIT_LOCAL_PATH, "assets", "img", "posts")
POST_ASSET_PATH = "/assets/img/posts"
VIDEO_ASSET_DIR = os.path.join(GIT_LOCAL_PATH, "assets", "video")
VIDEO_ASSET_PATH = "/assets/video"
AUDIO_ASSET_DIR = os.path.join(GIT_LOCAL_PATH, "assets", "audio")
AUDIO_ASSET_PATH = "/assets/audio"
IMG_TYPE = "png"
VIDEO_TYPE = "mp4"
AUDIO_TYPE = "mp3"

# content settings
MAX_IMG_WIDTH = 760
MAX_VIDEO_WIDTH = 760


# logging settings
LOG_LEVEL = os.getenv("LOG_LEVEL", "INFO")
LOG_FORMAT = os.getenv(
    "LOG_FORMAT",
    r"[%(levelname)s] - %(asctime)s -> %(message)s",
)

LOGGING_CONFIG = {
    "version": 1,
    "disable_existing_loggers": True,
    "formatters": {"default": {"format": (LOG_FORMAT), "datefmt": "%I:%M:%S"}},
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "level": LOG_LEVEL,
            "formatter": "default",
        }
    },
    "root": {"level": LOG_LEVEL, "handlers": ["console"]},
}

logging.config.dictConfig(LOGGING_CONFIG)
