import time
import schedule
import logging

from .bsky import create_post_from_most_recent_blog_post

log = logging.getLogger(__name__)

schedule.every().hour.do(create_post_from_most_recent_blog_post)


if __name__ == "__main__":
    log.info("Starting cron job")
    while True:
        schedule.run_pending()
        time.sleep(1)
