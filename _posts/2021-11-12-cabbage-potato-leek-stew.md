---
layout: post
title: Cabbage, potato, carrot, leek stew
date: 2021-11-12 22:00:29 -0400
categories: 
  - recipe
hide_title: false
---

I made this [stew recipe](https://cooking.nytimes.com/recipes/12371-cabbage-potato-and-leek-soup?action=click&module=Global%20Search%20Recipe%20Card&pgType=search&rank=13) tonight. Had a ton of cabbage + potatoes, and carrots from our CSA and needed something to do with them. Roasted cabbage and leeks in a sheet pan for 30 minutes instead of caramelizing them in the pan. Used olive oil not butter. Broth consisted of discarded stems of herbs, leeks, corn cobs, cabbage leaves, mushroom powder, cheese rinds and squash innards - detritus from summer cooking. Cooked for 2-3 hours on low. Served with fresh dill, chives, and grated parmesan.