---
layout: post
title: Paste Magazine's "10 Overlooked Electronic Albums in 2022"
date: 2022-11-03 11:22:00 -0400
categories:
  - music
  - press
  - jennifer-vanilla
hide_title: false
---
[Paste Magazine](https://www.pastemagazine.com/music/electronic-music/best-electronic-albums-in-2022/) listed _Castle In The Sky_ as one of the best electronic albums in 2022.


<center>
<a href="https://www.pastemagazine.com/music/electronic-music/best-electronic-albums-in-2022/" target="_blank"><img src="/assets/img/posts/2022-12-23-paste-mag.png " width="66%" alt="Paste Magazine List"/></a>
</center>

