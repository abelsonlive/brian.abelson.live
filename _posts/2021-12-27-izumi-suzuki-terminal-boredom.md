---
layout: post
title: Izumi Suzuki - Terminal Boredom
date: 2021-12-27 19:53:29 -0400
categories: 
  - book
hide_title: false
---

An inventive, irreverent collection of speculative sci-fi in which emphasis is placed on the emotional lives of the depressed, often addicted (sometimes alien) characters, rather than the technological specifics of their dystopian societies.

"There was no way anyone could live in a world like this with a fully functioning mind. You only found yourself feeling angry from morning until night. If she ended up joining some kind of political movement as a result, her mother and father would be upset. Using drugs, she told herself, was her way of being a good daughter."