---
layout: post
title: I added search functionality to this site using Lunr.js
summary: How to add search functionality to your Jekyll site with Lunr.js
date: 2025-01-07 16:05 -0500
categories:
- code
image: /assets/img/posts/2025-01-07-search-page-small.png
bsky_uri: at://did:plc:vtck23cxtnfk7tn7e6runi2n/app.bsky.feed.post/3lf6muttrut2c
---
Following [this simple tutorial](https://www.stephanmiller.com/static-site-search/), I added a [search page](/search) to this Jeykll-site.

It harnesses [Lunr.js](https://lunrjs.com/) under-the-hood. The cool thing about it is that, since Jekyll is a static-site generator, you can build the search index on each deploy using liquid templates. 

```js
{% raw %}
<script>
  // Template to generate the JSON to search
  window.store = {
    {% for post in site.pages %}
    {% if post.exclude_from_search != true %}
      "{{ post.url | slugify }}": {
        "title": "{{ post.title | xml_escape }}",
        "categories": "{{ post.categories | join: ' ' | xml_escape }}",
        "content": {{ post.content | strip_html | strip_newlines | jsonify }},
        "url": "{{ post.url | xml_escape }}"
      },
    {% endif %}
    {% endfor %}
    {% for post in site.posts %}
      "{{ post.url | slugify }}": {
        "title": "{{ post.title | xml_escape }}",
        "categories": "{{ post.categories | join: ' ' | xml_escape }}",
        "content": {{ post.content | strip_html | strip_newlines | jsonify }},
        "url": "{{ post.url | xml_escape }}"
      }
      {% unless forloop.last %},{% endunless %}
    {% endfor %}
  };
</script>
{% endraw %}
```

I added an "exclude_from_search" parameter to certain page's [front matter](https://jekyllrb.com/docs/front-matter/) so I can control what shows up in the search results.

This is mostly for my own usage, but [maybe if I blog more](https://brian.abelson.live/log/2025/01/02/daily-link-blog.html), it'll be useful to others.