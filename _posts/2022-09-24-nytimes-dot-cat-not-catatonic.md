---
layout: post
title: nytimes.cat no longer "cat'atonic"
date: 2022-09-24 12:22:00 -0400
categories: 
  - jokes
  - art
  - video
  - loop
hide_title: false
---

A well-preserved version of the formerly ["cat'atonic"](https://nypost.com/2015/04/30/new-york-times-parody-site-goes-catatonic/) nytimes.cat is now available on [the Internet Archive](https://web.archive.org/web/20150429235607/http://nytimes.cat/).


<center>
  <video width="85%" autoplay loop muted controls>
      <source src="/assets/video/2022-09-24-nytimes-dot-cat-small.mp4" type="video/mp4">
  </video>
</center>
