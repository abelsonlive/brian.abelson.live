---
layout: post
title: Kitty of the year, the game.
date: 2021-12-25 11:00:45 -0400
categories: 
  - lola
  - loop
  - video
video: "/assets/video/2021-12-25-koty-the-game-small.mp4"
hide_title: false
---
Amanda commissioned our friend [Blake Andrews](https://pbjabcusa.com/) to make a game about Lola called ["Kitty of the year."](https://yokemart.itch.io/kitty-of-the-year-for-bryan) Feed the cats until they fall asleep and wait for a special visit from Lola.
