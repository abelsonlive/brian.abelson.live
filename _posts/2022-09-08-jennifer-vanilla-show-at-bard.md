---
layout: post
title: Jennifer Vanilla @ Bard College
date: 2022-09-08 23:22:00 -0400
categories: 
  - music
  - video
  - show
  - jennifer-vanilla
hide_title: false
---

[Becca](https://instagram.com/@jennifervanilla) compiled some great footage from our show at Bard College on 9/8/2022.
It was their first official party of the year and everyone went hard 🥵. [Zenizen](https://instagram.com/zenizenzenizen) played a beautiful set that night as well!

<center>
<video width="66%" autoplay loop muted controls>
    <source src="/assets/video/2022-09-08-jennifer-vanilla-performance-at-bard-small.mp4" type="video/mp4">
</video>
</center>

