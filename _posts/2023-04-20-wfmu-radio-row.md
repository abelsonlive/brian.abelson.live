---
layout: post
title: WFMU Radio Row Mix
hide_title: false
date: 2023-04-20 00:22:00 -0400
categories:
  - dj
  - music
  - mix
image: /assets/img/posts/megamix-promo.png
mix:
  url: 'https://dada.abelson.live/mixes/brian-abelson-WFMU-megamix.mp3'
  selector: wfmu-megamix
  segments:
  - timestamp: '00:00:00'
    tracks:
    - artist: Billie Holiday
      title: This is Heaven
      url: https://www.youtube.com/watch?v=EzHmA1NLFVA
  - timestamp: '00:00:11'
    tracks:
    - artist: Dimples D.
      title: Sucker DJ's (I Will Survive) (Suckapella)
      url: https://www.discogs.com/release/1203216-Dimples-D-Sucker-DJs-I-Will-Survive
    - artist: Heidi Sabertooth
      title: Squails
      url: https://lostsoulenterprises.bandcamp.com/album/heidi-sabertooth-neu-u
  - timestamp: '00:03:35'
    tracks:
    - artist: Jung DJ
      title: 'Untitled #17'
      url: https://jungdeejay.bandcamp.com/album/q-archive
    - artist: Terje Isungset
      title: I Skodda
      url: https://terjeisungset.bandcamp.com/album/reise
    - artist: Heidi Sabertooth
      title: Squails
      url: https://lostsoulenterprises.bandcamp.com/album/heidi-sabertooth-neu-u
  - timestamp: '00:05:53'
    tracks:
    - artist: The Hacker
      title: Underwater Sequence
      url: https://the--hacker.bandcamp.com/album/le-th-tre-des-op-rations
    - artist: Ray 7 & Malik Alston
      title: I.D.F.D.F.I.
      url: https://www.traxsource.com/artist/313271/ray-7-and-malik-alston
  - timestamp: '00:09:24'
    tracks:
    - artist: Dopplereffekt
      title: Scientist
      url: https://cloneclassiccuts.bandcamp.com/album/gesamtkunstwerk
    - artist: Bufiman
      title: Bonus Beatz
      url: https://clonedistribution.com/release/44406?play=b473e8a0d6692884edb6cff44d88d3a8
  - timestamp: '00:10:53'
    tracks:
    - artist: Bufiman
      title: Bonus Beatz
      url: https://clonedistribution.com/release/44406?play=b473e8a0d6692884edb6cff44d88d3a8
  - timestamp: '00:11:11'
    tracks:
    - artist: Elysia Crampton
      title: Spot 6
      url: https://bleep.com/release/85308-elysia-crampton-spots-y-escupitajo?lang=en_US
  - timestamp: '00:11:19'
    tracks:
    - artist: Ryuchi Sakamoto
      title: Riot in Lagos (2019 Remastering)
      url: https://www.deezer.com/en/album/110485282
    - artist: Talking Heads
      title: Pull up the Roots
      url: https://www.youtube.com/watch?v=nYWt_U43M9Y
  - timestamp: '00:16:07'
    tracks:
    - artist: Those Guys
      title: Love, Love, Love (Balto Doo Doo Mix)
      url: https://www.beatport.com/track/love-love-love/73186
    - artist: Conclave
      title: Sunny (Acapella)
      url: https://conclavemusic.bandcamp.com/album/sunny
    - artist: The Other People Place
      title: Moonlight Rendezvous
      url: https://warprecords.bandcamp.com/album/lifestyles-of-the-laptop-caf
  - timestamp: '00:19:30'
    tracks:
    - artist: Yello
      title: Oh Yeah 'Oh Six (Booka Shade remix)
      url: https://www.discogs.com/release/842164-Yello-Oh-Yeah-Oh-Six-The-Remixes
    - artist: Perishing Thirst
      title: Desecr-8
      url: https://perishingthirst.bandcamp.com/album/pilgrims-of-the-rinde
  - timestamp: '00:21:12'
    tracks:
    - artist: Booka Shade & M.A.N.D.Y
      title: Oh Superman
      url: https://getmandy.bandcamp.com/album/m-a-n-d-y-vs-booka-shade-feat-laurie-anderson-o-superman
    - artist: Perishing Thirst
      title: Desecr-8
      url: https://perishingthirst.bandcamp.com/album/pilgrims-of-the-rinde
  - timestamp: '00:24:45'
    tracks:
    - artist: Booka Shade & M.A.N.D.Y
      title: Oh Superman
      url: https://getmandy.bandcamp.com/album/m-a-n-d-y-vs-booka-shade-feat-laurie-anderson-o-superman
    - artist: Acronym City
      title: The Push
      url: https://runningbackrecords.bandcamp.com/album/powermoves
  - timestamp: '00:25:35'
    tracks:
    - artist: Mark Williams
      title: Your Time
    - artist: Booka Shade & M.A.N.D.Y
      title: Oh Superman
      url: https://getmandy.bandcamp.com/album/m-a-n-d-y-vs-booka-shade-feat-laurie-anderson-o-superman
    - artist: Acronym City
      title: The Push
      url: https://runningbackrecords.bandcamp.com/album/powermoves
  - timestamp: '00:29:02'
    tracks:
    - artist: Huey Mnemonic
      title: Technician 2
      url: https://hueymnemonic.bandcamp.com/album/technician-w-max-watts-remix-ebon002
    - artist: Titonton Duvante
      title: Titillate
      url: https://dbh-music.bandcamp.com/album/rez005-the-pornographic-ep
  - timestamp: '00:32:50'
    tracks:
    - artist: Planetary Assault Systems
      title: The 808 Track, Parts 1 & 2
      url: https://tokenrecords.bandcamp.com/album/aphelion
    - artist: Sneaker
      title: Haunted Samba
      url: https://ratliferecords.bandcamp.com/album/algerian-ra
    - artist: Terrence Dixon
      title: Lock Out Chamber
      url: https://hardwax.com/69824/terrence-dixon/badge-of-honor/
  - timestamp: '00:37:10'
    tracks:
    - artist: Davis Galvin
      title: Rissp
      url: https://davisgalvin.bandcamp.com/track/rissp
    - artist: The Advent
      title: Kombination True
      url: https://www.youtube.com/watch?v=yDE_NeSVapY
  - timestamp: '00:40:49'
    tracks:
    - artist: Mad Professor & Lee Scratch Perry
      title: Techno Dub
      url: https://www.discogs.com/release/1064407-Mad-Professor-Lee-Perry-Techno-Dub
    - artist: pent
      title:  track7 (pent live)
      url: https://pent3.bandcamp.com/album/pent-live
  - timestamp: '00:43:15'
    tracks:
    - artist: DJ Swisha
      title:  If the Shoe Fits
      url: https://djswisha.bandcamp.com/album/clout-psychosis
    - artist: pent
      title:  track7 (pent live)
      url: https://pent3.bandcamp.com/album/pent-live
    - artist: Charanjit Singh
      title: Raga Bhairav
      url: https://www.youtube.com/watch?v=mzOfcu4SKlQ
  - timestamp: '00:47:10'
    tracks:
    - artist: Aux 88
      title: Alien Bounce
      url: https://www.beatport.com/track/alien-bounce/809965
    - artist: DJ K1
      title: Erase The Time
      url: https://www.youtube.com/watch?v=hYAntvERiJ4
  - timestamp: '00:50:38'
    tracks:
    - artist: The Knife
      title: Full of Fire
      url: https://theknife.bandcamp.com/album/shaking-the-habitual
  - timestamp: '00:55:12'
    tracks:
    - artist: Jung DJ
      title: 'Untitled #14'
      url: https://jungdeejay.bandcamp.com/album/q-archive
    - artist: Billie Holiday
      title: This is Heaven
      url: https://www.youtube.com/watch?v=EzHmA1NLFVA
    - artist: The Knife
      title: Full of Fire
      url: https://theknife.bandcamp.com/album/shaking-the-habitual
  - timestamp: '00:57:28'
    tracks:
    - artist: Billie Holiday
      title: This is Heaven
      url: https://www.youtube.com/watch?v=EzHmA1NLFVA
---

I'm excited to share a new mix I made for [Radio Row](https://wfmu.org/playlists/R1), a program run by Olivia Bradley-Skill, [WFMU's](https://wfmu.org/) Music Director. If you're interested in submitting your own show, you should fill out [this form](https://docs.google.com/forms/d/e/1FAIpQLSf88jwEiwjnNmv7Ug0TSgFWVtP3v4XTo0yo45ecQOVd8QHNfQ/viewform) as it's a great opportunity to play your favorite music on the airwaves.

I love WFMU. We listen to it daily in our house and I value it as a source of new music. For my submission, I wanted to showcase a style of mixing that is not often done on air, making use of DJ tools, acapellas, and live effects to create a multi-layered, ever-evolving blend of dance music, while also paying homage to some of the halcyon sounds WFMU DJs gravitate towards. It features many of my favorite songs and artists, and some of my closest friends and mentors.

It'll broadcast live this Sunday, April 23rd, at 5 PM ET on 91.9 FM in NYC and online at [wfmu.org](https://wfmu.org/)

I hope you like it <3.
