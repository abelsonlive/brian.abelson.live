---
layout: post
title: This website is upside-down
summary: I made this upside-down website as a part of auditing an @sfpc-study class.
date: 2025-01-06 15:55 -0500
categories:
- art
- code
- essays
image: /assets/img/posts/2025-01-06-upside-down-website-small.png
image_link: /projects/upside-down/
hide_title: false
published: true
bsky_uri: at://did:plc:vtck23cxtnfk7tn7e6runi2n/app.bsky.feed.post/3lf4iy2coeo23
---
As a part of auditing [Todd Anderson's](https://toddwords.com/) "code poetry" class at the [School For Poetic Computation](https://sfpc.study/) last year, I made an [upside-down website](/projects/upside-down/). The prompt was "Make a 'bad' website that breaks the rules."

At the time, I had just read [Ingrid Burrington's](https://lifewinning.com/) book, [_Networks of New York_](https://mhpbooks.com/books/networks-of-new-york/) and was thinking about the physicality of digital infrastructure. What small gestures can a website make to remind the viewer that what they are looking at is more than just a "website" but an accumulation of natural resources, human knowledge, and cultural norms?

Here's the full-text, right-side up:

# This website is upside-down

The internet is a digital space built on physical infrastructure.

Too often we consume information from the web in a mindless manner, executing a rote set of movements - swipes, clicks, keystrokes - learned from years spent interacting with familiar interfaces.

In the same way that an upside-down map changes our perception of the planet, and the relationship of the Global South and Global North, this website changes our perception of the internet by altering the way we physically interact with it.

To comfortably read this website, you're encouraged to flip your device upside-down.

Notice how, through this small gesture, your computer becomes more of an inanimate object and less of a portal to meaning and connection.