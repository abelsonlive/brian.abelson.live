---
layout: post
title: "Making your mac sing"
date: 2023-02-20 00:22:00 -0400
categories:
  - code
  - saysynth
  - music
  - open-source
hide_title: false
---

<center><a href="https://saysynth.org" target="_blank"><img src="/assets/img/saysynth-small.png" alt="Saysynth logo" width="69%"/></a></center>

When I was first introduced to a computer, my parents made it talk to me. I was transfixed by its text-to-speech capabilities and, throughout my childhood, joyously commanded it to "Start Speaking." Perhaps it was because I was ashamed of my own speech impediment — for which I was often bullied in school — but the awkward and monotonous cadence of its voice was endearing to me, and I spent countless hours playing with it.

Sometime in college I realized that you could, with your own voice, ask it "What time is it?" and it would respond in its synthesized drawl. I proceeded to make this part of my morning routine, much to the annoyance of my roommates, since the feature rarely worked and required finding the exact phrasing of the prompt.

As I began programming computers in my twenties, I discovered that you could control these voices in your terminal:

```shell
$ say "hello world" -v Fred
```

The say command [and its many options and voices](https://ss64.com/osx/say.html) deepened my fascination with text-so-speech. With each new technology I experimented with — [twitter bots](https://twitter.com/Buzzed_Feed), [data sonificaiton](https://www.youtube.com/watch?v=-8MIiZb-Oo8), [haiku generation](https://ohhaikubot.tumblr.com/), or [chat.meatspac.es](https://github.com/abelsonlive/cl-meats) - I would inevitably try piping the output of my programs into say. Just to experience, once again, the childlike wonder of my computer talking to me.

[saysynth](https://saysynth.org/saysynth.html) represents the outcome of a lifelong fascination with these synthesized voices, sparked by the discovery of an obscure documentation website for [Apple's Speech Synthesis Framework](https://developer.apple.com/library/archive/documentation/UserExperience/Conceptual/SpeechSynthesisProgrammingGuide/SpeechOverview/SpeechOverview.html#//apple_ref/doc/uid/TP40004365-CH3-SW6). [saysynth](https://saysynth.org/saysynth.html) works by harnessing a [domain-specific language](https://en.wikipedia.org/wiki/Domain-specific_language) (DSL) Apple created to allow users to control the duration and pitch contours of individual phonemes, enabling the creation of musical passages with three of Mac's built-in voices (Fred, Victoria, and Alex). By releasing it as open-source software, I hope to make it widely accessible to musicians and tinkerers alike.

But while I'm excited for people to play around and build on top of [saysynth](https://saysynth.org/saysynth.html), I'm happy if I end up being the only one who ever uses it. Sometimes the greatest joy in creation is satisfying your own curiosity.

I still have many features planned, including pitch modulation, real-time midi control, and hopefully a UI or interactive website. And with each release, I hope to create new demos! Give them a a listen and let me know what you think. xx

<center>
<iframe style="border: 0; width: 350px; height: 470px;" src="https://bandcamp.com/EmbeddedPlayer/album=846701542/size=large/bgcol=ffffff/linkcol=0687f5/tracklist=false/transparent=true/" seamless><a href="https://abelsonlive.bandcamp.com/album/saysynth-demos-v100">saysynth demos v1.0.0 by Brian Abelson</a></iframe>
</center>

