---
layout: post
title: "Winding up the Negril river"
date: 2023-01-21 11:22:00 -0400
categories:
- loop
- birds
- video
hide_title: false
---

Winding up the Negril river and coming upon an egret.


<center>
  <video width="85%" autoplay loop muted controls>
      <source src="/assets/video/2023-01-21-winding-up-the-negril-river-small.mp4" type="video/mp4">
  </video>
</center>
