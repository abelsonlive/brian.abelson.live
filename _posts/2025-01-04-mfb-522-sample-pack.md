---
layout: post
title: MFB-522 Sample Pack
date: 2025-01-04 12:47:00 -0500
hide_title: false
published: true
summary: I created a free sample pack of the MFB-522 Drumcomputer from synthlibrarynyc.org
image: /assets/img/posts/2025-01-04-mfb-522-small.png
bsky_uri: at://did:plc:vtck23cxtnfk7tn7e6runi2n/app.bsky.feed.post/3lexlnilhld2k
---
Over the last month, I borrowed the excellent [MFB-522 Drumcomputer](https://www.analoguehaven.com/mfb/mfb522/) from the [Synth Library](https://synthlibrarynyc.org/). The MFB-522 is an all-analogue drum machine that's quite similar to the Roland TR-808 except I've always thought the hi-hats sounded better.

I previously owned this unit, but felt more people would get use out of it if I gave it to the Synth Library. However, I always regretted not recording its sounds before I did. So when I saw it was available last month, I took to the opportunity to check it out :).

I ran the samples through a [SoundCraft mixer](https://www.soundcraft.com/en-US/products/FX16ii) with a bus that included a [Warm Audio WA273-EQ Pre-amp](https://warmaudio.com/mic-pre-wa73-eq) (basically a Neve 1073 clone) and a [FMR Audio RNC1773 compressor](https://fmraudio.com/rnc.html), which served mostly as a limiter. The samples were recorded in stereo but also exported in mono.

You can download the resulting sample pack from my new [samples page](/samples). I plan on sporadically adding more in the future.

Speaking of the Synth Library, I'll be hosting another [Floating Synth Room](https://lu.ma/uknhugug) later this month featuring the Sequential Circuits Pro-One. Hope to see you there!
