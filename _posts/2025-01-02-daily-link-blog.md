---
layout: post
title: Daily links in 2025
date: 2025-01-02 12:18 -0500
hide_title: false
published: true
categories:
- links
- music
- book
- film
---

This year, I'm going to try to use this blog more.  Inspired by Simon Willison's [daily link blog](https://simonwillison.net/2024/Dec/22/link-blog/), I'm going to try to at least post one thing here a day. It may be a link, it may be a song, it may be a movie or book. It probably won't involve much introspection or analysis. The point, as Simon puts it, is that:

> the value is in writing frequently and having something to show for it over time—worthwhile even if you don’t attract much of an audience (or any audience at all).

Watching: [Perfect Blue](https://en.wikipedia.org/wiki/Perfect_Blue)

Listening: [John Beltran - _Ten Days of Blue_](https://music.apple.com/us/album/ten-days-of-blue/260240898)

Seeing: [Ayanna Heaven and Loum Present Sounds](https://www.eventcreate.com/e/ayannaloum)

Reading: [Kaliane Bradley - _The Ministry of Time_](https://bookshop.org/p/books/the-ministry-of-time-kaliane-bradley/20696241)
