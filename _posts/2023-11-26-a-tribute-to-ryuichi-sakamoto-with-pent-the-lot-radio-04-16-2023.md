---
layout: post
title: a tribute to Ryuichi Sakamoto with pent @ The Lot Radio 04-16-2023
date: 2023-11-26 19:49:29 -0500
categories:
- mix
hide_title: false
published: true
---
Caught a replay of this mix on [The Lot Radio](https://thelotradio.com) this afternoon and noting it here for the archive :) 

<iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1494658360&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/thelotradio" title="The Lot Radio" target="_blank" style="color: #cccccc; text-decoration: none;">The Lot Radio</a> · <a href="https://soundcloud.com/thelotradio/a-tribute-to-ryuichi-sakamoto-with-pent-the-lot-radio-04-16-2023" title="a tribute to Ryuichi Sakamoto with pent @ The Lot Radio 04-16-2023" target="_blank" style="color: #cccccc; text-decoration: none;">a tribute to Ryuichi Sakamoto with pent @ The Lot Radio 04-16-2023</a></div>
