---
layout: post
title: The Alley Pond Giant
date: 2021-12-28 23:58:00 -0400
categories: 
  - trees
hide_title: false
---
Today I walked to the Alley Pond Giant: A 350-400 year old Tulip Tree which is supposedly "the oldest living organism in New York City."

<p style="text-align: center"><img style="width: 66%; object-fit: contain;" src="/assets/img/posts/2021-12-29-alley-pond-giant-small.jpg" alt="The Alley Pond Giant"></p>


[Nestled on a steep slope near the intersection of the Long Island and Cross Island Expressways](https://www.atlasobscura.com/places/the-alley-pond-giant-queens-new-york), the tree has survived because the land it sprouted from was of no use to the city that sprawled around it. 

<p style="text-align: center;"><img style="width: 60%; object-fit: contain;" src="/assets/img/posts/2021-12-28-alley-pond-giant-map-small.png" alt="The Alley Pond Giant - Map"></p>