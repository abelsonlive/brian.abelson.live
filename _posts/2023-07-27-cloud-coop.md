---
layout: post
title: Cloud Co-op
hide_title: false
published: false
date: 2023-07-26 00:24:00 -0400
categories:
  - essays
  - mutual-aid
---

Back towards the end of 2021, I wrote [a short essay](/log/2021/11/12/thoughts-on-mutual-aid.html) reflecting on my first year working as the coordinator for [Bushwick Ayuda Mutua's](https://bushwickayudamutua.com/) Tech and Data Support Working Group. In that post, I asked a series of questions which I'm still grappling today:

> If mutual aid is ultimately concerned with solidarity, then should it do so by relying on the charity of capitalist endeavors? What would it even look like to perform these services without the infrastructure that these companies singularly provide?

At the time, much of our work was still subsidized by the tech industry's various COVID-19 relief programs. As I correctly worried, that money has dried up and mutual-aid networks and other community organizations are no  longer the poster children of PR campaigns, but simply customers on free or cheap paid plans.  Support requests now go unanswered and blog posts championing our use of their tools go unwritten. I had previously believed that "building alternative communities of care and supporting our neighbors [was] too important to overly worry about which tools we're using to do it." I no longer think that's the case.

"A sustainable city needs the option of an organic Internet. One whose infrastructure is built, owned, controlled, and maintained by local communities."


links
- http://solarprotocol.net/
- https://link.springer.com/chapter/10.1007/978-3-319-66592-4_13#Sec3








