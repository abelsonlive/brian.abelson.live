---
layout: post
title: Sonicare loops
date: 2023-11-20 19:22:04.876145
categories:
- loop
- music
- video
hide_title: false
published: true
---
Been having fun making drones with my electric toothbrush. Opening and closing your mouth while using it functions kind of like an LFO/low pass filter.

<center>
<video width="66%" autoplay loop muted controls>
    <source src="/assets/video/2023-11-20-trim-fb2ff6ca-9e8f-466d-b7f6-d03c389dac85-small.mp4" type="video/mp4">
</video>
</center>