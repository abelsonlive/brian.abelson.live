---
layout: post
title: "\u201COne never walks when one can possibly ride\u201D"
date: 2023-11-21 21:00:20.958691
categories:
- notes
- transportation
hide_title: false
published: true
---
<center>
<img src="/assets/img/posts/2023-11-21-7ec349c6-3260-43b8-8c34-292091b887fe-small.png" width="90%"/>
</center>
Quote from “Langley’s City Directory” about commuting in San Francisco circa 1867. At the Cable Car Museum.