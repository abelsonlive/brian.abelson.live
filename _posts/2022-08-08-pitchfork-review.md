---
layout: post
title: Pitchfork Review
date: 2022-08-08 09:22:00 -0400
categories: 
  - music
  - press
hide_title: false
---
It's weird to have _Castle in the Sky_ reviewed in Pitchfork. As someone who was interested in music and came of age in the mid-to-late aughts, it was so influential on my tastes and my aspirations, not always for the better. I'm not sure what it means to be in Pitchfork now, but a small perhaps vain side of me felt validated by it – though that was fleeting. Here's to keeping your head down and making good work.

<center>
<a href="https://pitchfork.com/reviews/albums/jennifer-vanilla-castle-in-the-sky/" target="_blank"><img src="/assets/img/posts/2022-08-08-pitchfork-review-small.png" width="50%" alt="Pitchfork review"/></a>
</center>
