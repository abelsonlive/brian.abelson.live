---
layout: post
title: 'Ayanna Heaven: Light and Sound Design, 2025-01-02'
date: 2025-01-03 10:52:00 -0500
hide_title: false
published: true
categories:
- links
- music
- show
---

[Light and Sound Design](https://www.lightandsound.design/) is a treasure. One of the few remaining DIY spaces in the city, it features a vintage sound system sourced in part from [Magick City](http://magick.city/) (RIP) and custom light installations built by the volunteers that run the space. Ascending the LED strip-lit stairs last night, Amanda and I came to see [Ayanna Heaven](https://www.instagram.com/ayannaheaven/), a local DJ and radio host on [WKCR](https://www.cc-seas.columbia.edu/wkcr/) who I booked last summer to play [Solar System](https://solarsystem.nyc/).

LS&D is a communal effort and their Thursday shows - _Present Sounds_ - feature meals made by volunteers for purchase on an honor system. Last night, Kim made an excellent Kimchi stew sourced from homemade Kimchi to which he added sunflower seeds because he "likes the crunch." While _Present Sounds_ tends to highlight experimental and ambient artists, Ayanna focused on Dub, Reggae, Funk, R&B, and Soul. I was surprised that many people fell asleep on the mattresses laid out on the floor, as my head was continually nodding throughout the set. 

Here were some of the highlights for me:

- [Bobby Hutcherson - Montara (The Roots Remix)](https://music.apple.com/us/song/montara-the-roots-remix/1556725228)
- [Dorothy Ashby - Come Live With Me](https://music.apple.com/us/song/come-live-with-me/1443482193)
- [Hiatus Kaiyote - Sparkle Tape Break Up (Mndsgn Remix)](https://music.apple.com/us/song/sparkle-tape-break-up-mndsgn-remix/1610329169)
- [Augustus Pablo - Say So](https://music.apple.com/us/song/say-so/1533708054)
- [Nu Genea - Stasis (Calm Before the Storm) [feat. Tony Allen]](https://music.apple.com/us/song/stasis-calm-before-the-storm-feat-tony-allen/1086571728)
