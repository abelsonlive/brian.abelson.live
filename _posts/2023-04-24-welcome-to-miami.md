---
layout: post
title: welcome-to.miami
hide_title: false
date: 2023-04-24 00:22:00 -0400
video: "/assets/video/welcome-to-miami.mp4"
categories:
  - loop
  - video
  - art
---

While migrating my old Splice projects before [they shutdown their Studio tool](https://splice.com/blog/studio-shutdown/) (RIP), I unearthed a screen recording of a pair of sites I built a few years ago, "welcome-to.miami" and "bienvenidos-a.miami". While each site played a short loop of the music video by @willsmith with the relevant lyric, "welcome-to.miami" would redirect to "bienvenidos-a.miami" and "bienvenidos-a.miami" would redirect back and also open a new tab of "welcome-to.miami" such that the number of tabs grew exponentially, causing a cacophonous, phasing effect. I eventually took the site down once browsers disabled autoplay on video. Happy to have found documentation of it :)
