---
layout: post
title: Kokoroko playlisted _Castle in the Sky_
date: 2022-08-05 09:22:00 -0400
categories: 
  - music
  - press
hide_title: false
---
Kokoroko wrote a nice blurb on _Castle in the Sky_ for the German publication, [Music Expresse](https://www.musikexpress.de/).

<center>
<a href="https://www.musikexpress.de/me-alben-der-woche-kokoroko-agajon-jennifer-vanilla-2181431/" target="_blank"><img src="/assets/img/posts/2022-08-05-kokoroko-playlist-header-small.png" width="67%" alt="Kokoroko blurb"/></a>
<a href="https://www.musikexpress.de/me-alben-der-woche-kokoroko-agajon-jennifer-vanilla-2181431/" target="_blank"><img src="/assets/img/posts/2022-08-05-kokoroko-playlist-txt-small.png" width="67%" alt="Kokoroko blurb"/></a>
</center>
