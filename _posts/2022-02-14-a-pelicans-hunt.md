---
layout: post
title: A Pelican's Hunt
date: 2022-02-14 16:58:00 -0400
categories: 
  - loop
  - video
  - birds
video: "/assets/video/2022-02-14-pelican-small.mp4"
hide_title: false
---
On a beach in the Yucatan, I quietly watched as Pelicans fished. Floating serenely in the waves, they suddenly took off, seemingly abandoning their prey, only to loop back around, ride an updraft into the sky, and nose dive into the water.
