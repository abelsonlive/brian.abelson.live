---
layout: post
title: 'Green Curry #1'
date: 2025-02-22 20:03 -0500
summary: I tried to make green curry. I should've put the vegetables in at different
  times.
categories:
- recipe
hide_title: false
published: true
---
I made green curry tonight using store-bought curry paste and following [this recipe](https://www.recipetineats.com/thai-green-curry/).

Some notes:
- I minced cilantro stalks and garlic and sauteed them with the curry paste.
- I added a cup of my own veggie broth. I wish I had added a little less as it came out a little watery.
- I included eggplant, red peppers, carrots, and tofu. I should've added ingredients in this order:
    * tofu (10 min total)
    * eggplant (5 min total)
    * peppers + carrots (2 min total)
- I also think I should've just let the curry simmer for 5-10 minutes on its own to let the flavors develop before adding the veggies and tofu.