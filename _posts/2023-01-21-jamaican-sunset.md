---
layout: post
title: "Jamaican sunset"
date: 2023-01-21 11:22:00 -0400
categories:
- loop
- video
hide_title: false
---

Watching the sun slowly dip into the water.

<center>
  <video width="85%" autoplay loop muted controls>
      <source src="/assets/video/2023-01-21-jamaican-sunset-timelapse-small.mp4" type="video/mp4">
  </video>
</center>
