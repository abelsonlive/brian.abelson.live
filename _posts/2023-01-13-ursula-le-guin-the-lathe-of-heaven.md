---
layout: post
title: "[Book] Ursula K. Le Guin: _The Lathe of Heaven_"
date: 2023-01-13 11:22:00 -0400
categories:
- book
hide_title: false
---

“Love doesn't just sit there, like a stone, it has to be made, like bread; re-made all the time, made new. When it was made, they lay in each other's arms, holding love, asleep.”
