---
layout: post
title: Whither the Pageview Apocalypse?
date: 2013-10-01 22:00:29 -0400
categories:
  - jokes
  - essays
  - apocalypticism
hide_title: false
---
<br/>
<center><img width="75%" src="/assets/img/posts/2013-10-01-apocalypse-header.jpg"/></center>

> ”Wild, dark times are rumbling toward us, and the prophet who wishes to write a new apocalypse will have to invent entirely new beasts, and beasts so terrible that the ancient animal symbols of St. John will seem like cooing doves and cupids in comparison.” — Heinrich Heine, Lutetia; or, ‘Paris’, Augsberg Gazette, 1842

The pageview is dead. Jeff Jarvis presided over its wake in 2007, soberly preparing us for a brave new world of Flash, AJAX, and embeddable widgets in which a page was no longer just a page. Chartbeat announced its death as early as 2012 and most recently in a sponsored post for the Online News Association’s upcoming conference. My current position as a 2013 Knight-Mozilla OpenNews Fellow was granted a messianic status when some equated the fellowship to a violent crusade against the metric.

Yet, when I open Google Analytics, I am presented with a time series chart of pageviews. When I visit newsrooms, I often see a ‘big board’ listing the top ten articles by pageviews. And when I sit in analytics meetings, I regularly hear the metric tossed around as a means of benchmarking one article against another. If, as I’ve been led to believe, this is a post-pageview world, then we must be living in a zombie apocalypse as I’m relentlessly haunted by the metric’s lifeless corpse.

What then of the pageview apocalypse and the prophets who giddily proclaim it? To what ends are these revelations leading us? What strategic aims and benefits are these claims predicated upon?

——

In Jacques Derrida’s 1982 essay, Of an Apocalyptic Tone Newly Adopted in Philosophy, he writes of a tendency prevalent in academia in which scholars paradoxically announce the ‘death’ or ‘end’ of their fields. This same tendency can be located in art and culture when critics bemoan the death of Hip-Hop or Punk. The last decade of the News Industry has often resembled the final minutes of Reservoir Dogs, with publications announcing the demise of their counterparts only to be shot down themselves in the following frame. There is even an online newspaper dedicated to the death of newspapers.

For Derrida, though, such apocalyptic declarations are intriguing not for the end they depict, but for the transformative visions embedded within their rhetoric. Grounding his discussion in etymology, ‘apocalypse’ is derived from the Greek apokalupsis which translates to “reveal” or to “uncover.” In the Hebrew Bible, the equivalent word gala is used over a hundred times saying in effect “disclosure, uncovering, unveiling, the veil lifted from about the thing,” most often in reference to the sex and/or genitalia of a man or woman, but also in reference to their sensory organs (eyes, ears, mouth). ‘Apocalypse’, then, literally means the act of uncovering: the removing of clothes, the shifting of hair, or the unveiling of eyes to reveal a secret or unknowable existence just beyond the surface. In this manner, an apocalypse is “essentially a contemplation,” or a meditation upon a veiled state, which is structured by a desire of a particular disclosure or revelation to its thought process.

——

So what is being revealed when the death of the pageview is proclaimed? Our first clue comes from the coroners. More often than not, they are the stewards of Web Analytics, an industry that was largely built upon measuring pageviews. From Chartbeat, to KISSmetrics, to WebTrends, and Nielsen, it seems like every analytics company has shared in the apocalyptic glee. Mixpanel, a particularly brazen analytics startup, even purchased billboard space along Highway 101 to announce the death of the pageview:

<center>
<img width="75%" src="/assets/img/posts/2013-10-01-apocalypse-billboard.png"/>
</center>

Remind you of anything?

<center>
<img width="75%" src="/assets/img/posts/2013-10-01-apocalypse-billboard-christian.jpg"/>
</center>

Why then are so many analytics companies taking up arms against themselves? What is the meaning of this cargo cult of counter-analytics? Here, we can use the common structure of the above billboards as our guide:

- Loudly announce the end.
- Suggest a counter-action.

In almost every case, when we are told the pageview is dead, we are given a list of metrics that will take its place. This is the revelation. Instead of pageviews, we’re advised, we should be quantifying engagement, trying out A/B tests, conducting funnel analyses, or deploying click/event tracking on our sites (conveniently enough, these tools have often just been added to the next iteration of their platforms). And it’s not that these metrics are useless — they can be of great value when designed and deployed correctly — it’s that, in lieu of a critical assessment of how and why pageview-centric platforms failed us, we are instead told that our egotism led us to pay attention to the wrong things in the first place.

Yet, having experimented with many “actionable”, rather than ”vanity metrics”, I can tell you that their results are often just as murky and misleading. Engagement is a moving target; A/B tests, when poorly designed, often produce inconclusive results; event tracking, while incredibly powerful, does not readily enable comparisons across varied contexts. And, even when these tools are utilized to their full potential, it can be very difficult to translate their insights into action. The fact of the matter is that there are no silver bullets, no secrets to be revealed just beyond the pageview. All there is is hard work, open dialogue, and relentless experimentation to find what works in your particular context. After all, we’re talking about measuring the complex behaviors of millions of people.

Still, many will try and seduce you into believing otherwise. This act of seduction, Derrida explains, is the principal strategy of apocalypticism:

> ”the subject of [apocalyptic] discourse can have an interest in forgoing its own interest, can forgo everything in order to place yet its death on your shoulders and make you inherit in advance its corpse, that is, its soul, the subject hoping thus to arrive at its end through the end” (52).

In this powerful, if enigmatic passage, we begin to understand the true motivations of apocalyptic prophets. Doomsayers do not merely seek acknowledgement of an end-to-come or one that has already passed, they are more concerned with seducing you into accepting the terms on which their continued existence, their vested interests, and their vision of ‘the end’ are all equally possible. It’s not that analytics platforms are flawed, they say, it’s simply that you’re not paying attention to the right parts; It’s not that insights are difficult, they promise, it’s that you’ve been going about finding them in the wrong way.

So when we are told that the ‘pageview is dead’, what we are actually being told is that ‘analytics platforms are dying’; that the current paradigm is fundamentally flawed and that the companies responsible are scrambling to convince us otherwise. And, rather than accepting their share of the blame, they cite our inherent egotism — our blindly narcissistic desire for validation — in a plot to absolve themselves of guilt and justify their importance. In this vision of the apocalypse, Babylon is inhabited by the users of metrics, not their marketers or makers.

——

> “This is the way the world ends. Not with a bang but a whimper.” — T.S. Elliot, The Hollow Men, 1925.

While the big data bubble has inflated quickly, it will not simply ‘pop’. So, instead of worrying about whether we’re measuring the wrong things, or using the wrong tools or software, or falling behind the competition, let’s take a deep breath, ignore the doomsayers, and do the best we can with what we have right now. And, if after a while, that’s still not working, then perhaps we should reassess precisely why, in what manner, and by whom we were convinced that analytics would solve our problems in the first place.