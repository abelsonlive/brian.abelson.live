---
layout: post
title: Why make AI music when there are random arpreggiators?
date: 2025-03-09 12:23 -0400
summary: Why make AI music when there are random arpreggiators?
categories:
- loop
- music
hide_title: false
published: true
video: /assets/video/2025-03-09--small.mp4
bsky_uri: at://did:plc:vtck23cxtnfk7tn7e6runi2n/app.bsky.feed.post/3ljxjnjfuuy2j
---
