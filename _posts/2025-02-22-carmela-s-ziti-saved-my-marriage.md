---
layout: post
title: Carmela's Ziti Saved My Marriage
date: 2025-02-22 20:18 -0500
summary: ''
categories:
- recipe
hide_title: false
published: true
image: /assets/img/posts/2025-02-23--small.png
bsky_uri: at://did:plc:vtck23cxtnfk7tn7e6runi2n/app.bsky.feed.post/3lisqhej73r2b
---
For valentines day, I made ziti for Amanda inspired by [Carmela Soprano's recipe](https://imturning60help.blogspot.com/2012/08/carmela-sopranos-ziti-al-forno.html). I found it on this amazing blogspot - [imturning60help.blogspot.com](https://imturning60help.blogspot.com) - which mostly consists of recipes reposted from a Soprano's cookbook.

I skipped all the meat stuff and made a simple sauce based on [Marcela Hazan's tomato sauce](https://www.foodandwine.com/recipes/tomato-sauce-onion-and-butter)  using a whole bottle of Mutti. I wish there had been a bit more sauce.