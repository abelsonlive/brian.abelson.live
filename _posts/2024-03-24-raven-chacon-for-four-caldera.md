---
layout: post
title: Raven Chacon - For Four (Caldera)
date: 2024-03-24 17:54:00 -0400
categories:
- art
hide_title: false
published: true
image: /assets/img/posts/2024-03-24-9a95d530-9d7f-4e69-aaaf-02114bf2f930-small.png
video: /assets/video/2024-03-24-trim-01954801-914c-4d69-8425-33780bfc6144-small.mp4
---
I was first introduced to Raven Chacon at [Dronefest](https://basilicahudson.org/events/24-hour-drone-2023/) last year when he performed a transfixing three hour noise set involving [hyper-directional speakers](https://www.ultrasonic-audio.com/products/acouspade-directional-speaker/). By manually adjusting their position, often at high speeds, he created disorienting effects of laser beams or overdriven insects buzzing around the circular space. I had never seen or heard anything like it and instantly became a fan.

I’ve since seen one other of his compositions performed in which a quartet soundtracked one of his graphical scores for which he is well known.

This piece, currently on display at [the Swiss Institute in NYC](https://www.swissinstitute.net/exhibition/raven-chacon-a-worms-eye-view-from-a-birds-beak/), trades a graphical score for the shapes of the rolling hills surrounding the Valles Caldera in New Mexico, which was formed 1.25 million years ago when a Volcano erupted (it’s also 15 miles from the location of the Los Alamos National Laboratory where the Atomic Bomb was developed).

In the piece, four singers are positioned around a small pond, each facing in a different direction. The “singers slowly rotate and while scanning the horizon line, singing the contour of the landscape.” The resulting sounds are haunting and beautiful, as the overlapping voices gradually shift between harmony and dissonance, evoking the tension between the tranquility of the landscape and the violence of the forces that created it.
