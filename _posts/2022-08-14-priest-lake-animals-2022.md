---
layout: post
title: Priest Lake Animals 2022
date: 2022-08-14 09:22:00 -0400
categories: 
  - priest-lake
  - list
hide_title: false
---
Here's the list of animals we saw in Priest Lake this year:

- Skunk
- Bat
- Chipmunk
- Bunny
- Lake Trout
- Deer
- Otter
- Bald eagle
- Osprey
- Pups
- Crow
- Ducks
- Geese
- Hummingbird
- Sparrow
- Horse
