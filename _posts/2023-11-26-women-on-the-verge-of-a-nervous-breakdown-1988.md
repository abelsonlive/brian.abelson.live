---
layout: post
title: Women on the Verge of a Nervous Breakdown (1988)
date: 2023-11-26 16:02:56 -0500
categories:
- film
hide_title: false
published: true
---
Watched Pedro Almodovar's _[Women on the Verge of a Nervous Breakdown](https://en.wikipedia.org/wiki/Women_on_the_Verge_of_a_Nervous_Breakdown)_. Enjoyed the intro sequence with Ivan's voiceover teamed with the fact that he only interacts with Pepa via voice messages (including their shared work dubbing English films in Spanish) until the end of the film. It's as if some men treat relationships as nothing more than a series of cliché lines cribbed from pop culture. I saw Almodovar's recent short with Tilda Swinton, _[The Human Voice](https://en.wikipedia.org/wiki/The_Human_Voice_(film))_, earlier this year and it clearly drew upon this earlier film, including the vibrant palette of reds, greens, and blues.