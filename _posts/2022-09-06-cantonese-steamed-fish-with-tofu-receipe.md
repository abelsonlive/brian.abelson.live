---
layout: post
title: Cantonese Steamed Fish with Tofu
date: 2022-09-06 23:22:00 -0400
categories: 
  - recipe
hide_title: false
---

I made this [cantonese steamed fish recipe](https://thewoksoflife.com/cantonese-steamed-fish/) last night but substituted tofu for the white fish.


### Ingredients

- 2 scallions
- 3 tablespoons ginger
- 1 small bunch cilantro
- 3 tablespoons light soy sauce (or seasoned soy sauce)
- 1/4 teaspoon salt
- 1/4 teaspoon sugar
- 4 tablespoons hot water
- 16 ounces of tofu cut into 8 1.5 in strips
- 2 tablespoons vegetable oil

### Instructions

- Cut the scallions into 2-inch lengths, and cut the pieces in half lengthwise. Julienne them thinly. Thinly slice about 15g of ginger, and julienne them. Give the cilantro a rough chop. Set the aromatics aside.
- Combine the light soy sauce, salt, sugar and hot water in a small bowl and mix until the sugar and salt are dissolved. Set aside.
- Prepare your steaming set-up, and fill with 1-2 inches of the mixture above, reserve the rest.
- Carefully place tofu in the steamer, and adjust the heat to medium. The mixture should be at a slow boil that generates a good amount of steam, but not so high that the water evaporates too quickly.
- Cover and steam for 7-10 minutes depending upon the size and thickness of your tofu.
- Turn off the heat, and carefully drain any liquid on the plate. Spread about ⅓ of the scallions, ginger, and cilantro on the steamed tofu(alternatively, you can wait to do this AFTER adding the sauce).
- To make the sauce, heat a wok or small saucepan to medium high heat, and add 2 tablespoons vegetable oil. Add the remaining ⅔ of the ginger, and fry for 1 minute. Add the white parts of the scallions and cook for 30 seconds.Then add rest of the scallions and cilantro. The mixture should be sizzling.
- Add the soy sauce mixture. Bring the mixture to a bubble, and cook until the scallions and cilantro are just wilted, about 30 seconds.
Pour this mixture over the tofu. If you prefer to add the raw aromatics after adding the sauce, you can do so now, and heat an additional 1 tablespoon of vegetable oil to pour over the raw aromatics. Serve immediately!
