---
layout: post
title: Whither the AI Apocalypse?
hide_title: false
published: true
date: 2023-05-31 00:24:00 -0400
categories:
  - essays
  - AI
  - apocalypticism
---

Ten years ago, I wrote [an essay](/log/2021/11/12/whither-the-pageview-apocalypse.html) for my [Open News Fellowship](https://www.opennews.org/what/fellowships/info/) which critically assessed the Web Analytics industry's proclamation that "the pageview is dead." The essay got some traction online and was eventually used as a framework for [analyzing a right-wing politician's use of the "rapture" when criticizing Obama's foreign policy](https://www.salon.com/2013/10/09/the_end_of_pageviews_and_michele_bachmanns_rapture/). The gist of the argument, which borrowed from Jacques Derrida's 1982 essay [_Of an Apocalyptic Tone Recently Adopted In Philosophy_](/assets/pdf/derrida-of-an-apocalyptic-tone-recently-adopted-in-philosophy.pdf), was that doomsayers are primarily "concerned with seducing you into accepting the terms on which their continued existence, their vested interests, and their vision of ‘the end’ are all equally possible." As Derrida concisely put it: "the subject of [apocalyptic] discourse [hopes] to arrive at its end through the end." A decade later, I can confidently say that the pageview is very much still alive and that [the leaders of the analytics companies who predicted otherwise are now very rich](https://venturebeat.com/data-infrastructure/app-analytics-provider-mixpanel-becomes-latest-unicorn/).

––

Yesterday, [a collection of academic and industry leaders collectively signed a statement](https://www.safe.ai/statement-on-ai-risk) which issued this stark, apocalyptic proclamation:

> Mitigating the risk of extinction from A.I. should be a global priority alongside other societal-scale risks, such as pandemics and nuclear war.

The statement implies that since AI is a "societal-scale" risk, governmental regulation on the scale of responses to other apocalyptic threats is required (note their blatant omission of climate change).  By positioning themselves as "AI experts," it also puts them in a natural position to help craft these regulations given the complexity of the technology. While they are purposefully vague on what an AI apocalypse might look like, most doomsday scenarios follow the concept of the ["singularity"](https://en.wikipedia.org/wiki/Technological_singularity) in which AI undergoes "a 'runaway reaction' of self-improvement cycles... causing an 'explosion' in intelligence and resulting in a powerful superintelligence that...far surpasses all human intelligence." As the story goes, this superintelligence, no longer moored by its creators, begins to act in its own interests and eventually wipes out humanity.  A particularly sophisticated and influential example of this narrative [was published on the online forum LessWrong last year](https://www.lesswrong.com/posts/pRkFkzwKZ2zfa3R6H/without-specific-countermeasures-the-easiest-path-to).

––

Earlier this month, an interesting document leaked which was confirmed to have originated from a Google AI researcher. The essay, entitled [_We Have No Moat, And Neither Does OpenAI_](https://www.semianalysis.com/p/google-we-have-no-moat-and-neither), traces the rapid development of open-source alternatives to OpenAI's and Google's [large language models (LLMs)](https://en.wikipedia.org/wiki/Large_language_model) over the past few months. It argues that the open-source community is much better equipped to push the forefront of LLM research because it's more nimble, less bound by bureaucratic inefficiencies, and has adopted tools like ["Low-Rank Adaptation"](https://arxiv.org/abs/2106.09685) for fine-turning models without the need of large clusters of GPUs. As the author summarizes:

> We have no secret sauce. Our best hope is to learn from and collaborate with what others are doing outside Google.
>
> People will not pay for a restricted model when free, unrestricted alternatives are comparable in quality. We should consider where our value add really is.
>
> Giant models are slowing us down. In the long run, the best models are the ones which can be iterated upon quickly.

For anyone who has been closely following the development of open-source tooling for LLMs over the past couple of months, these assertions are not particularly controversial. Each day brings a fresh crop of tweets and blog posts announcing a new software package, model, hosting platform, or technique for advancing LLM development. These tools are then quickly adopted and iterated upon leading to the next day's announcements. In many ways, this rapid advancement mirrors the 'runaway reaction' at the heart of the singularity narrative.

––

If we understand apocalyptic narratives as a rhetorical sleight-of-hand, then we must ask the same questions of the AI industry [which my previous essay asked of Web Analytics](/log/2021/11/12/whither-the-pageview-apocalypse.html):

> What then of the [AI] apocalypse and the prophets who giddily proclaim it? To what ends are these revelations leading us? What strategic aims and benefits are these claims predicated upon?

Given the arguments outlined in the leaked document above (and backed up by anecdotal evidence) we can conclude that the "existential threat" AI companies are most concerned with is their inability to profit from the nascent ["AI boom."](https://www.nytimes.com/2023/05/31/magazine/ai-start-up-accelerator-san-francisco.html) [Google is particularly vulnerable to this threat](https://www.nytimes.com/2023/04/16/technology/google-search-engine-ai.html) given its heavy reliance on search-related advertising which [reportedly accounts for more thant 80% of its yearly revenue](https://www.cnbc.com/2021/05/18/how-does-google-make-money-advertising-business-breakdown-.html). Regulation, while absolutely necessary, could be shaped in a way to potentially stifle the rapid development of the open-source community by requiring complex security, privacy, or other restrictions thereby making it illegal (or at least cost prohibitive) for an individual to develop an LLM on their laptop. Since many "AI experts" directly work for, or receive funding from the companies that signed the letter, it stands to reason that the aim of this proclamation is to ensure that they have a hand in shaping the eventual regulations in a way which ensures their monopolistic domination of the AI industry.

––

I have a different take on the “singularity” which is informed by recently completing [_Palo Alto_](https://www.amazon.com/Palo-Alto-History-California-Capitalism/dp/031659203X) – an excellent history of American capitalism _vis-a-vis_ the tech industry.  The basic idea is that capitalists have long sought to replace employees with automation and, for those tasks they can’t fully automate, to make the workers performing them function more like machines. In this reading, the "singularity" is not achieved simply by making machines "sentient," but by simultaneously turning humans into machines, effectively lowering the bar an AI has to jump over to achieve sentience; if you work in an Amazon Fulfillment Center, you are already functioning very close to a machine, and much of your work is probably focused on training [the robots that will replace you](https://www.youtube.com/watch?v=r2VcA7nMJs8&vl=en). Keep in mind that the crucial difference between GPT-3 and ChatGPT was the addition of [reinforcement learning from human feedback](https://huggingface.co/blog/rlhf) (or R.L.H.F) [which involved underpaying and overworking Kenyan contractors to make the model "less toxic"](https://time.com/6247678/openai-chatgpt-kenya-workers/) (this exploitative process is a ["joke"](https://www.nytimes.com/2023/05/30/technology/shoggoth-meme-ai.html) for AI researchers). That the AI industry's development will likely coincide with a rise in factory-like conditions for those asked to train the models is no less of a disastrous scenario, but at least in this reading we can rightfully point the finger at the capitalists for the "end of the world" rather than AI. To modify William Gibson's adage ([which might not actually be his](https://www.nytimes.com/2012/01/15/books/review/distrust-that-particular-flavor-by-william-gibson-book-review.html)): "the apocalypse is already here it's just not evenly distributed."
