---
layout: post
title: Rough, grey water
date: 2021-11-13 22:00:29 -0400
categories: 
  - loop
  - video
hide_title: false
video: "/assets/video/2021-11-13-waves-small.mp4"
---
Walked out to the pier of [Shirley Chisholm State Park](https://www.google.com/maps/place/Shirley+Chisholm+State+Park/@40.6378325,-73.8685773,191m/data=!3m1!1e3!4m5!3m4!1s0x89c2613ef1180eb7:0xf6af241cd5804dca!8m2!3d40.6427737!4d-73.8748443) with Amanda a few minutes before a severe thunderstorm. The sea was rough and grey and the wind roared in our ears.

