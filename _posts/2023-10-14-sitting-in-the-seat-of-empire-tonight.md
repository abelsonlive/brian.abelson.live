---
layout: post
title: Sitting in the seat of empire tonight.
hide_title: false
published: true
date: 2023-10-14 00:24:00 -0400
categories:
  - essays
  - free-palestine
---

Sitting in the seat of empire tonight, I rest comfortably in my quiet home, under the soft glow of dimmed lights, listening to music, cuddling my cat, drinking fresh water, and staring at a flat plane of glass informing me of a genocide in progress. Like many sitting in the the seat of empire tonight, this feeling is all too familiar. The window into this atrocity is smaller, the voices reporting it are those of my friends, the pleas to politicans are automated, but the sense of powerlessness remains.

I scroll through social media until my device tells me I've reached my daily limit. I call my representatives and Cisco Systems tells me their inboxes are full. I make donations which my credit card flags as fraud. I march in the street and am surrounded by police in riot gear. The systems of power which secure my comfort, which provide me with food and water and entertainment, also prevent me from taking meaningful action. The systems of power which secure my comfort are committing genocide on my behalf, without my consent.

What is there to do? What can be done? I tell myself to not give up hope, to not look away, to follow the guides and click the links in the Google Docs. I dream of walking door-to-door, like some trick-or-treater dressed up as Thomas Jefferson, pleading with my neighbors to "Free Palestine." I fantasize of standing on a busy corner holding up a sign or writing an urgent call to action in my company Slack. Maybe I'll even change my profile picture.

Tomorrow, I will wake up, lying in bed, and thousands of Palestinians will be lying dead under blood-stained sheets. Maybe I shouldn't sit so comfortably in the seat of empire tonight.
