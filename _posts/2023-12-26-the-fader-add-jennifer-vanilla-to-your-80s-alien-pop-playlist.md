---
layout: post
title: "The Fader: \"Add Jennifer Vanilla to your 1980's alien-pop playlist\""
date: 2023-12-26 21:03:06 -0500
categories:
- press
- jennifer-vanilla
hide_title: false
published: true
---
[A nice review](https://www.thefader.com/2023/12/05/jennifer-vanillas-castle-in-the-sky-is-more-than-cosmic-kitsch) of _Castle in the Sky_ was published in _The Fader_ earlier this month:

> With enough curated cheeriness and kitschy synth work, any artist worth their salt can emulate an ’80s sound. But Castles In the Sky takes on the much trickier task of blending the era’s capitalist excesses with its radical elements — art that arose as a response to the brutal injustices of austerity and imperialism masked as American exceptionalism — and projecting the whole mess onto our current technocratic dystopia. Jennifer Vanilla’s dimension of origin is an eternal ’80s, one in which the decade’s false promises are suspended in midair like floating castles. But Grant’s critical eye complicates the premise, letting the weight of the future seep ever so slightly into their avatar’s perpetual daydream.