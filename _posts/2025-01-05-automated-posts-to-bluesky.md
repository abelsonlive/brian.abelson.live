---
layout: post
title: Automated posts to Bluesky
date: 2025-01-05 07:36 -0500
summary: My blog posts are now automatically sent to Bluesky
categories:
- code
hide_title: false
published: true
bsky_uri: at://did:plc:vtck23cxtnfk7tn7e6runi2n/app.bsky.feed.post/3leyrdv64n52k
---
I was interested in re-engaging with my prior Twitter habit without falling down a rabbit hole every time I went to post something. Since [I'm trying to blog more](https://brian.abelson.live/log/2025/01/02/daily-link-blog.html), I figured I could send summaries of my posts to [Bluesky](https://bsky.app/profile/abelsonlive.bsky.social).

Bluesky has an [easy-to-use API](https://docs.bsky.app/) with [good documentation](https://atp.readthedocs.io/en/latest/atproto_client/auth.html). It doesn't even require registering an OAuth app – you just login with your username and password - which reminds me of how simple it was to make Twitter bots back in the day.

Building upon the [CMS](https://brian.abelson.live/log/2023/11/26/this-site-now-has-a-cms.html) I built for this blog, I added a [script](https://gitlab.com/abelsonlive/brian.abelson.live/-/blob/main/cms/cms/bsky.py) to post my most-recent entry to Bluesky. The script keeps track of which posts are already on Bluesky by adding a "bsky_uri" parameter to the post's [front matter](https://jekyllrb.com/docs/front-matter/). I used Python's [schedule](https://schedule.readthedocs.io/en/stable/index.html) library to [run it every hour](https://gitlab.com/abelsonlive/brian.abelson.live/-/blob/main/cms/cms/cron.py).
