---
layout: post
title: "globally.ltd/studio"
date: 2023-03-08 00:22:00 -0400
categories:
  - code
  - globally-limited
  - music
hide_title: false
---
<p>
<center>
<a href="https://globally.ltd/studio"><img src="https://globally.ltd/assets/images/studio-a.jpg" width="69%"/></a>
</center>
</p>

As a part of releasing software and music for the last couple of years on my label  [globally.ltd](https://globally.ltd), I've slowly built up a functioning recording studio in my basement and I'm excited to now open it to others.

[globally.ltd/studio](https://globally.ltd/studio/) is a small, project-based art and recording studio in Glendale, Queens offering a variety of services including in-person (solo or supervised) sessions and remote mixing, mastering, and mentorship.

Rates will be give-what-you-can, with a suggested range of $15-30/hr, though bartering and skill-sharing is preferred!

You can read more about it at: [globally.ltd/studio/](https://globally.ltd/studio/).
