---
layout: post
title: Live life with mo' egrets
date: 2023-01-21 11:22:00 -0400
categories:
- loop
- birds
- video
hide_title: false
---

<center>
  <video width="85%" autoplay loop muted controls>
      <source src="/assets/video/2023-01-21-live-life-with-mo-egrets-small.mp4" type="video/mp4">
  </video>
</center>
