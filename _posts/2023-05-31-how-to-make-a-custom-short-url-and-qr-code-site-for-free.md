---
layout: post
title: How to make a custom short URL and qr code site for free.
hide_title: false
date: 2023-05-31 00:22:00 -0400
categories:
  - code
  - mutual-aid
  - open-source
---

TLDR; The source code is [here](https://github.com/bushwickayudamutua/baml.ink).

As a part of its regular operations, [Bushwick Ayudua Mutua](https://bushwickayudamutua.com/) makes regular use of short URLs and QR codes to share links to assistance request forms, volunteer sign-up forms, and other important information hosted online.

Like many small groups, BAM relied on bit.ly and a bevy other QR-code generation platforms which either cost a lot of money (bit.ly is $30/month) or harvest your data, exposing community members to potential privacy violations.

However, the underlying technology to create short URLs and QR codes is fairly simple and can be easily replicated using [urlzap](https://github.com/brunoluiz/urlzap/), python's [qrcode](https://pypi.org/project/qrcode/) package, and GitHub's [Actions](https://github.com/features/actions) and [Pages](https://pages.github.com/) products.

I packaged these tools together into [baml.ink](https://github.com/bushwickayudamutua/baml.ink) which can be forked and reconfigured to create your own, fully-static short URL / QR code generation service.

## How link shortening works:

The repository contains a human-readable/editable yaml file (named "baml.yaml" :p ) which looks something like this:

```yaml
urls:
  ig: https://www.instagram.com/bushwickayudamutua/
```

Here, "ig" is the path of short link, so you could then then share "baml.ink/ig" and it would point to "https://www.instagram.com/bushwickayudamutua/".  Each time this file is updated in the "main" branch, a GitHub Action is triggered which runs the [urlzap GitHub Action](https://github.com/brunoluiz/urlzap-github-action) which fetches the metadata for the long url and creates a static HTML file which includes the following meta tag in the "<head>" of the document:

```html
<meta http-equiv="refresh" content="0; url=https://www.instagram.com/bushwickayudamutua/" />
```

This tells browsers to redirect the visitor to "https://www.instagram.com/bushwickayudamutua/". By also including the metadata which is present on the source page, the short URL will appear normally when [unfurled](https://medium.com/slack-developer-blog/everything-you-ever-wanted-to-know-about-unfurling-but-were-afraid-to-ask-or-how-to-make-your-e64b4bb9254) within messaging apps.

This static HTML file is then added to the "gh-pages" branch of the repository and deployed as a new page hosted by GitHub. You can see a full example of such a file [here](https://github.com/bushwickayudamutua/baml.ink/tree/gh-pages/home).

## How QR code generation works:

In addition to the link shortening process, [an additional script](https://github.com/bushwickayudamutua/baml.ink/blob/main/generate_qr_codes.py) is executed each time there is an update to the "main" branch which iterates through the list of URLs in "baml.yaml", generates a QR code for the short URL, and writes it to a the "qr/" directory of the repository. These images take the format: "https://baml.ink/qr/{short_path}.png". So, given the Instagram example above, the QR code would be hosted at [baml.ink/qr/ig.png](https://baml.ink/qr/ig.png).

## How non-coders can use this:

While this project must be hosted on GitHub to remain free, by centralizing all short URL and QR code generation into a single file, volunteers can use GitHub's built-in code editor to easily add new URLs to "baml.yaml" and commit their changes, all without ever opening a terminal or cloning a repository. These changes are then automatically applied via GitHub Actions such that they should see their short URL and QR code go live within minutes.

