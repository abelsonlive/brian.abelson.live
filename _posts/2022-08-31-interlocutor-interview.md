---
layout: post
title: Interlocutor Interview
date: 2022-08-31 23:22:00 -0400
categories: 
  - music
  - press
  - jennifer-vanilla
hide_title: false
---
[Interlocutor interviewed](https://interlocutorinterviews.com/new-blog/2022/8/23/jennifer-vanilla-interview-castle-in-the-sky-sinderlyn-records-becca-kauffman) Becca and I on our recent album, _Castle In The Sky_.

<a href="https://interlocutorinterviews.com/new-blog/2022/8/23/jennifer-vanilla-interview-castle-in-the-sky-sinderlyn-records-becca-kauffman" target="_blank"><img src="/assets/img/posts/2022-08-31-interlocutor-interview-small.png" width="100%" alt="Interlocutor Interview"/></a>

Here's a segment:

_Your new album Castle in the Sky is the culmination of three years of working the songs out with live audiences and tweaking them based on audience vibes, in close collaboration with co-writer/co-producer Brian Abelson.Throughout this period of experimentation, did you both have any set plans of eventually turning all of this into a full album, or was there no “grand plan” and did it ultimately come together organically?_
* JV: On that note, now’s a good time to bring Brian into this interview.

* Brian: While the album came together in bits and pieces over a long period of time, we always conceived of it as a cohesive narrative rather than a collection of songs. Early on in the process, Becca and I were really into the idea of world-building as applied to sound and wanted to try and build a sonic universe that Jennifer Vanilla might reside in. You'll notice that many tracks share vocal refrains, sound effects, and other elements which serve to tie them together.

* JV: Like Brian said, we were working to develop a kind of language or thematic texture—a soundtrack—to the Jennifer Vanilla experience. It was an exercise in figuring out how the expression, attitude, and personality of the persona and the live performance could translate into sound, and Brian’s technical fluency and creative drive made that possible. After probably a couple hundred hours, we accumulated an album’s worth of material, and here we are.

_A unique production aspect of Castle in the Sky is that live performers recorded for it (including Teeny Lieberson [Lou Tides, TEEN, Here We Go Magic] on guitar, Boshra Al Saa-di [TEEN, Saadi] on bass, and Thesan Polyanna on saxophone), but then Brian kept the production in an electronic space by using the recordings as samples on the tracks. How do you think this approach serves the album's overall sonic aesthetic while also best fitting its themes and tones?_

* Brian: One of the main themes of the album is identity crisis—who am I? Is the face I present to the world the real me or just a performance? During the creation of this album, Becca and I were occupied with these questions both in our personal lives and as artists. Becca was just in the process of finishing up over a decade with Ava Luna and I was trying to branch out from working strictly on dance music.

    So, when we first came together, we had some trouble landing on a sonic aesthetic as we were both in a state of flux. While we initially gravitated towards sample-based electronic productions as that's what we were into at the time, the Black Lives Matter movement helped us understand the problem of uncritically reproducing or sampling queer black music as white artists.

    Bringing in live performers (and recording our own live performances) was a way to push against this historical pattern of cultural appropriation and help us carve out a sound and style of our own, drawing not only on our experiences in dance music but also Becca's long history recording and performing with bands. Ultimately, we felt this approach was not only a more respectful way to pay homage to the many artists we drew inspiration from, but also a more honest reflection of our artistic identities.