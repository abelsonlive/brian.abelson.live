---
layout: post
title: Murmuration in Highland Park
date: 2023-12-11 11:02:56 -0500
categories:
- loop
- video
- birds
hide_title: false
published: true
---

Walking through Highland Park, a flock of starlings swarmed overhead.  

<center>
<video width="90%" autoplay loop muted controls>
    <source src="/assets/video/2023-12-11-img-4680-small.mp4" type="video/mp4">
</video>
</center>
