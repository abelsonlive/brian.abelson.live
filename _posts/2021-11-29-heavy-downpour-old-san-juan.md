---
layout: post
title: Heavy Downpour In Old San Juan
date: 2021-11-29 11:00:29 -0400
categories: 
  - loop
  - video
hide_title: false
video: "/assets/video/2021-11-29-san-juan-rain-small.mp4"
---
Walking through Old San Juan on Thanksgiving Day, Amanda and I got stuck in a tropical downpour. Hiding under the awning of a hotel, we watched as large drops of rain turned the road into a river.
