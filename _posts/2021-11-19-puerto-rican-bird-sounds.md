---
layout: post
title: The words of birds
date: 2021-11-19 14:00:00 -0400
categories: 
  - list
  - birds
hide_title: false
---
I recently purchased a copy of _A Guide To The Birds of Puerto Rico And The Virgin Islands_ at [Rodgers Book Barn](http://www.rodgersbookbarn.com/).

<p style="text-align: center"><img src="/assets/img/posts/pr-birds.jpg" width="275px" alt="A Guide To The Birds of Puerto Rico And The Virgin Islands"></p>

I particularly enjoyed its onomatopoetic descriptions of bird noises. Here's a random list of my favorites:

- A raspy _crick-et_ (White-tailed Tropicbird)
- A hoarse _kak_ (Brown Booby)
- A guttural _ga-ga-ga-ga_ (Red-Footed Booby)
- A peculiar _oong-ka-chunk!_ (American Bittern)
- A raspy, insect-like _kay-ti-dad_ (Gull-Billed Tern)
- Plaintive, liquid, twittering notes (Audobon's Shearwater)
- A raspy _ke-aaar_ that fades away (Red-tail hawk)
- A high-pitched _killi-killi-killi_ (American Kestrel)
- A short, emphatic _bow-wow_ (Short-eared Owl)
- A throaty _ka-ka-ka-ka-ka-ka-ka-ka-ka-kaw-kow, kow, kow, kow_ (Yellow-billed Cuckoo)
- A very loud and conspicuous squawky whistle _a-leep_ (Smooth-billed Ani)
- A varied assortment of gentle cooing notes (Rock Dove)
- A deep, deliberate _whoo_ (Plain Pigeon)
- A raucous squawk (Brown-throated Parakeet)

