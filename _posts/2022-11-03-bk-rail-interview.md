---
layout: post
title: Brooklyn Rail Interview with Jennifer Vanilla
date: 2022-11-03 11:22:00 -0400
categories:
  - music
  - press
  - jennifer-vanilla
hide_title: false
---
[Brooklyn Rail interviewed](https://interlocutorinterviews.com/new-blog/2022/8/23/jennifer-vanilla-interview-castle-in-the-sky-sinderlyn-records-becca-kauffman) Becca and included some nice words on _Castle In The Sky_:


<center>
<a href="https://interlocutorinterviews.com/new-blog/2022/8/23/jennifer-vanilla-interview-castle-in-the-sky-sinderlyn-records-becca-kauffman" target="_blank"><img src="/assets/img/posts/2022-11-03-jennifer-vanilla-in-bk-rail-small.png" width="66%" alt="Brooklyn Rail Interview"/></a>
</center>
