---
layout: post
title: Joe Delon - Top Releases of 2023
date: 2024-01-06 13:14:33 -0500
categories:
- music
- press
hide_title: false
published: true
image: /assets/img/posts/2024-01-06-screenshot-2024-01-06-at-1-12-46-pm-small.png
---
Joe Delon included [John and I's release last year](https://johnbareraandbrianabelson.bandcamp.com/album/the-arc) in his [2023 roundup](https://joedelon.substack.com/p/top-2023-part-4) and gave us a kind writeup. Always nice to be included in the same breath with anything [Davis Galvin](https://davisgalvin.bandcamp.com/) does.