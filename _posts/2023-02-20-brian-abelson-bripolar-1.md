---
layout: post
title: "bripolar #1"
date: 2023-02-20 00:23:00 -0400
categories:
  - music
hide_title: false
---


<center>
<iframe style="border: 0; width: 350px; height: 470px;" src="https://bandcamp.com/EmbeddedPlayer/album=1876167600/size=large/bgcol=ffffff/linkcol=0687f5/tracklist=true/transparent=true/" seamless><a href="https://abelsonlive.bandcamp.com/album/bripolar-1">bripolar #1 by Brian Abelson</a></iframe>
</center>
<br/>
<hr/>
<br/>

I'm pleased to be releasing [bripolar #1](https://abelsonlive.bandcamp.com/album/bripolar-1), the first in a collection of mostly live takes of hard, industrial techno. It's been fun rigging up my mixer with multiple sends and bus channels to really achieve a rich, ever-evolving sound. More of these to come...

<br/>

<center><img src="/assets/img/posts/bripolar-studio-small.jpg" width="50%"/></center>
