---
layout: post
title: Nicholas Wirth - A plea for lean software (1995)
date: 2023-11-21 18:15:06.316511
categories:
- notes
- green-software
hide_title: false
published: true
---
I initially came across [this article](https://ia801608.us.archive.org/8/items/pdfy-PeRDID4QHBNfcH7s/LeanSoftware_text.pdf) through a citation in [Software Sustainability](https://link.springer.com/book/10.1007/978-3-030-69970-3), a collection of articles on the topic of "Green in Software" - generally how to make software more sustainable by reducing its carbon footprint. The article referenced "Wirth's Law" which is:

> Software is getting slower more rapidly than hardware becomes faster.

This can be contrasted with ["Moore's Law"](https://en.wikipedia.org/wiki/Moore's_law), the better-known observation that the complexity of computers doubles every two years. 

Together, these two "laws" form ["Jevon's Paradox"](https://en.wikipedia.org/wiki/Jevons_paradox) as appled to computing, in which increases in the energy efficiency of computers (defined as the proportion of processing power outputted to energy inputted) have lowered costs and thereby increased demand, negating the efficiency gains. This paradox was initially observed after Watt's steam engine, which made coal more cost-effective, led to the increased use of steam engines, and therefore coal, as well.
