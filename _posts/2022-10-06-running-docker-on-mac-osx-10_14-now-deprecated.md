---
layout: post
title: How to run docker-compose on Mac OSX 10.14 (Mojave) without Docker Desktop
date: 2022-10-06 12:22:00 -0400
categories:
  - code
  - tutorial
hide_title: false
published: true
---

I run Mac OSX version 10.14 (Mojave) because some audio software I use doesn't support newer versions. Unfortunately, that version of OSX is now [deprecated](https://www.docker.com/blog/what-you-need-to-know-about-macos-x-10-14-deprecation/) by Docker Desktop leaving luddites in the dark.

It turns out you can still run a Docker daemon via **[colima](https://github.com/abiosoft/colima)**.

All you need to do is:

```bash
$ brew install colima docker # install dependencies
$ colima start --runtime docker # start the docker runtime
$ docker ps # check that its running
$ curl -L https://github.com/docker/compose/releases/download/v2.11.2/docker-compose-darwin-x86_64 \
$   -o /usr/local/bin/docker-compose # download appropriate docker-compose binary from github
$ chmod +x  /usr/local/bin/docker-compose # make it executable
$ docker-compose # check that this worked
```

Now you can use **docker-compose** on it's own!
