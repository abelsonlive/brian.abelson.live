---
layout: post
title: Jennifer Vanilla @ Brooklyn Steel 10/5/2022
date: 2022-10-05 12:22:00 -0400
categories: 
  - music
  - show
  - jennifer-vanilla
hide_title: false
published: true
---

What a surreal experience playing for nearly 2000 people at Brooklyn Steel.


<p style="text-align: center"><img style="width: 69%; object-fit: contain;" src="/assets/img/posts/2022-10-015-jennifer-vanilla-at-brooklyn-steel-crowd-small.jpg " alt="The Crowd at Brooklyn Steel"></p>

Here are some clips Becca compiled:


<center>
<video width="69%" autoplay loop muted controls>
    <source src="/assets/video/2022-10-05-jennifer-vanilla-at-brooklyn-steel-small.mp4" type="video/mp4">
</video>
</center>
