---
layout: post
title: This site now has a CMS
date: 2023-11-26 18:11:29 -0500
categories:
- code
- open-source
hide_title: false
published: true
image: /assets/img/posts/2023-11-26-87bovo-small.png
---
I've been trying to use social media less and this site more. The hope is to keep a more thorough, short-form log of stuff I've read, watched, made, eaten, thought about, etc. However, since the site is a [Jeykll](https://jekyllrb.com/)-backed static site, I previously had to be at my laptop to post a new entry.

While there are more [sophisticated options](https://www.siteleaf.com/) for managing Jekyll sites, I really just wanted to simple webform to post a new entry which I could access from my phone.  

After a few hours of hacking, I put together a simple [FastAPI](https://fastapi.tiangolo.com/) endpoint which performs the following operations:

- Accepts a title, list of categories, post body, an image or video, and an API key for security. 
- Pulls the latest version of [this site's repo](https://gitlab.com/abelsonlive/brian.abelson.live) from GitLab.
- Resizes the image and/or video using **ffmpeg**
- Generates a jekyll-formatted markdown file and writes it to the appropriate directory.
- Commits the changes and pushes them back to GitLab.

Since this site uses [GitLab CI](https://docs.gitlab.com/ee/ci/) to publish new changes, the new post shows up in 1-2 minutes.

Some things I'd like to add in the future:

- Auto-dithering of images to reduce page size like [lowtech magazine](https://solar.lowtechmagazine.com/2023/06/rebuilding-a-solar-powered-website/). (On the note of reducing page size, there's some pretty minimal Javascript running that uses JQuery which I could probably scrap).
- Add an "edit" button to each log entry so I can fix typos, or even have log entries that grow over time.
- Some sort of comment-via-email functionality? (this is half-baked).

Here are links to [the form](https://brian.abelson.live/cms/) and [the source code](https://gitlab.com/abelsonlive/brian.abelson.live/-/tree/main/cms).
