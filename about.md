---
layout: post
title: About
permalink: /about/
hide_title: true
---

<center><img style="width: 43%; height: 43%; object-fit: contain; border-radius: 50%;" src="/assets/img/profile-2.jpg" alt="Brian Abelson's Headshot"></center>

Hi, I'm Brian Abelson. I'm a [musician, programmer, and community organizer](/work/) in New York City and serve as the volunteer coordinator for [Bushwick Ayuda Mutua's](https://bushwickayudamutua.com) Tech and Data Support Working Group. I'm also a board member of [Raft Foundation](https://raft.foundation/).

- <a href="https://instagram.com/abelsonlive" target="_blank">**instagram**</a>
- <a href="https://bsky.app/profile/abelsonlive.bsky.social" target="_blank">**bsky**</a>
- <a href="https://abelsonlive.bandcamp.com/" target="_blank">**bandcamp**</a>
- <a href="https://soundcloud.com/abelsonlive" target="_blank">**soundcloud**</a>
- **email**: brian {at} abelson {dot} live
- **rss**: <a href="/feed.xml" target="_blank">feed.xml</a>

### Recent RSS feeds I've subscribed to:
<div id="about-recent-rss-feeds" class="feederrs"></div>

### Recently articles I've liked:
<div id="about-recent-starred-articles" class="feederrs"></div>

### Events I'm hosting at my studio
<iframe
  src="https://lu.ma/embed/calendar/cal-BmtGUTiHcLNXfsc/events?lt=dark"
  width="100%"
  height="300px"
  frameborder="0"
  allowfullscreen=""
  aria-hidden="false"
  tabindex="0"
></iframe>
