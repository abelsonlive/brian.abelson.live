start:

	bundle exec jekyll serve

resize_assets:

	echo "Creating -small versions of all assets"
	echo "======================================="
	./scripts/resize_assets.py
	echo "======================================="
	echo "Done!"

post:

	bundle exec jekyll post $(title)

resize_asset:

	cd cms && python -m cms.util ${file}
