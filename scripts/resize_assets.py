#!/usr/bin/env python3

import os
import copy

MAX_IMG_WIDTH = 960
MAX_VIDEO_WIDTH = 800

def listdir(dirname):
    return [
        os.path.join(dp, f)
        for dp, dn, fn in os.walk(os.path.expanduser(dirname))
        for f in fn
    ]


def main():
    for filename in listdir("assets/"):
        if (
            filename.endswith(".png")
            or filename.endswith(".jpg")
            or filename.endswith(".mp4")
        ) and "-small" not in filename:
            filetype = "." + filename.split(".")[-1]
            small_filename = filename.replace(filetype, f"-small{filetype}")
            if os.path.exists(small_filename):
                continue

            max_width = copy.copy(MAX_IMG_WIDTH)
            if filename.endswith(".mp4"):
                max_width = copy.copy(MAX_VIDEO_WIDTH)
            cmd = f"ffmpeg -i {filename} -vf scale={max_width}:-1 {small_filename}"
            print("="*60)
            print(f"Executing:\n{cmd}")
            os.system(cmd)


if __name__ == "__main__":
    main()
