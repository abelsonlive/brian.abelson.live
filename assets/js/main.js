$(function () {

    // COlORING
    var o = {
        x: -10,
        y: 20
    };
    $(document).mousemove(function (n) {
        o.x = n.pageX, o.y = n.pageY
    });
    var n = function () {
        var n = o.y / (($(window).height() + $(window).width()) / 2) * 360,
            c = "hsla(" + n + ", 50%, 75%, 1)";
        return c
    };

    var n2 = function () {
        var n = o.x / (($(window).height() + $(window).width()) / 2) * 360,
            c = "hsla(" + n + ", 50%, 75%, 1)";
        return c
    };

    $("a").hover(function () {
        $(this).css("color", n())
    }, function () {
        $("a").css("color", "white");
    })
    $("span.work-tag").hover(function () {
        $(this).css("color", "grey")
        $(this).css("cursor", "pointer")
        var currentText = $(this).text()
        $(this).text("#" + currentText);
    }, function () {
        $(this).css("color", "white");
        var currentText = $(this).text()
        $(this).text(currentText.substring(1));
    })
    $("span.work-tag").click(function () {
        var tagText = $(this).text().substring(1)
        $('tr.work-row').not('tr.' + tagText).fadeOut();
        var url = window.location;
        var url = url + "#" + tagText;
        window.location.replace(url);
    })
    $(".work-refresh-button").click(function () {
        var url = window.location.href;
        var parts = url.split('#');
        window.location.replace(parts[0]);
    })
    $("tr.work-row").not("span.work-tag").hover(function () {
        $(this).css("color", n())
    }, function () {
        $(this).css("color", "white");
    })

    // on clcking tr.work-row, open the link in data-url
    $("td.work-row-url-included").click(function () {
        var url = $(this).attr("data-url");
        window.open(url);
    })

    $("button.mix-segment-link").click(function () {
        var src = $(this).attr("data-src");
        var segment = $(this).attr("data-segment");
        var selector = $(this).attr("data-selector");
        var audioPlayer = $("#" + selector);
        var audioPlayerSource = $("#" + selector + " source");
        var tssrc = src + "#t=" + segment;
        $(audioPlayerSource).attr("src", tssrc);
        audioPlayer.trigger("pause");
        audioPlayer.trigger("load");
        audioPlayer.trigger("play");
    })
    // nav bar active


    $(document).ready(function () {
        var loc = window.location.href; // grabbing the url
        var path = loc.split("/")[3]; // splitting the url and taking the third string
        if (path == "") {
            $("#home").addClass("active");
        } else {
            if (path.startsWith("log")) {
                path = "home"
            }
            $("#" + path).addClass("active");
        }

        const feederrsURL = "https://feederss.abelson.live/data.json";
        var rssDiv = document.getElementById("about-recent-rss-feeds");
        var starredDiv = document.getElementById("about-recent-starred-articles");
        if (rssDiv != null && starredDiv != null) {
            $.getJSON(feederrsURL, function (data) {
                var newFeeds = data.new_feeds;
                var rssHtml = "<ul class='feederss-list'>"
                for (var i = 0; i < 5; i++) {
                    var feed = newFeeds[i];
                    if (feed.user_name!="brian") {
                        continue;
                    }
                    rssHtml += "<li>";
                    rssHtml += "<a class='feederss-link' href='" + feed.site_url + "' target='_blank'>";
                    rssHtml += "<img src='" + feed.feed_icon + "' width='16' height='16' alt='" + feed.feed_title + "' style='vertical-align: middle; margin-right: 0.5em;' />";
                    if (feed.feed_title.length > 40) {
                        rssHtml +=  feed.feed_title.substring(0, 50) + "...";
                    }
                    else {
                        rssHtml += feed.feed_title;
                    }
                    rssHtml += "</a>";
                    rssHtml += "</li>";
                }
                rssHtml += "</ul>";
                rssDiv.innerHTML = rssHtml;
                // new starred articles
                var newStars = data.new_stars;
                var starredHtml = "<ul class='feederss-list'>"
                for (var i = 0; i < 5; i++) {
                    var star = newStars[i];
                    if (star.user_name!="brian") {
                        continue;
                    }
                    starredHtml += "<li>";
                    starredHtml += "<a class='feederss-link' href='" + star.site_url + "' target='_blank'>";
                    starredHtml += "<img src='" + star.feed_icon + "' width='16' height='16' alt='" + star.feed_title + "' style='vertical-align: middle; margin-right: 0.5em;' />";
                    starredHtml += "</a>";
                    starredHtml += "<a class='feederss-link' href='" + star.entry_url + "' target='_blank'>";
                    starredHtml += star.entry_title + " – " + star.entry_author;
                    starredHtml += "</a>";
                    starredHtml += "</li>";
                }

                starredHtml += "</ul>";
                starredDiv.innerHTML = starredHtml;
            });
        };
    });
});
