---
layout: post
title: Search Results
hide_title: true
---
{% include search-box.html %}
<!-- List where search results will be rendered -->

<ul id="search-results"></ul>

<script>
  // Template to generate the JSON to search
  window.store = {
    {% for post in site.pages %}
    {% if post.exclude_from_search != true %}
      "{{ post.url | slugify }}": {
        "title": "{{ post.title | xml_escape }}",
        "categories": "{{ post.categories | join: ' ' | xml_escape }}",
        "content": {{ post.content | strip_html | strip_newlines | jsonify }},
        "url": "{{ post.url | xml_escape }}"
      },
    {% endif %}
    {% endfor %}
    {% for post in site.posts %}
      "{{ post.url | slugify }}": {
        "title": "{{ post.title | xml_escape }}",
        "categories": "{{ post.categories | join: ' ' | xml_escape }}",
        "content": {{ post.content | strip_html | strip_newlines | jsonify }},
        "url": "{{ post.url | xml_escape }}"
      }
      {% unless forloop.last %},{% endunless %}
    {% endfor %}
  };
</script>

<!-- Import lunr.js from unpkg.com -->
<script src="https://unpkg.com/lunr/lunr.js"></script>
<!-- Custom search script which we will create below -->
<script src="/assets/js/search.js"></script>