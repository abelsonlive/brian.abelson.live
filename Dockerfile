# This Dockerfile is for running the automation API in ./app
FROM python:3.11.4-buster

# set env
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8
ENV DEBIAN_FRONTEND=noninteractive

# Install system requirements
RUN apt update -y
RUN apt install -y \
	curl \
	build-essential \
	python3-pip \
	uwsgi \
	uwsgi-plugin-python3 \
	ffmpeg \
	git

# get pip striaght
RUN pip3.11 install --upgrade pip

# Configure system
RUN export PATH=~/.local/bin:$PATH
ENV PYTHONPATH "/opt/cms:$PYTHONPATH"

# Install API requirements

WORKDIR /opt/cms

#
COPY cms/requirements.txt /opt/cms/requirements.txt
RUN pip3.11 install -r /opt/cms/requirements.txt

# Install cms.
ADD cms /opt/cms/
ADD cms/run.sh /opt/cms/run.sh

# Start app
CMD ["/opt/cms/run.sh"]
